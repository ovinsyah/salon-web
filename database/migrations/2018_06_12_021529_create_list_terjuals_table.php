<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListTerjualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_terjuals', function (Blueprint $table) {
           $table->increments('id');
            $table->unsignedInteger('layanan_id');
            $table->unsignedInteger('terjual_id');
            $table->integer('qty');
            $table->timestamps();

            $table->foreign('layanan_id')
            ->references('id')
            ->on('layanans')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_terjuals');
    }
}
