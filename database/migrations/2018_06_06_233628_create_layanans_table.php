<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pemilik_id');
            $table->string('nama');
            $table->string('sampul');
            $table->integer('harga');
            $table->boolean('diskon');
            $table->string('gender');
            $table->longText('deskripsi');
            $table->timestamps();

            $table->foreign('pemilik_id')
            ->references('id')
            ->on('pemiliks')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layanans');
    }
}
