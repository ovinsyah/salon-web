<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemiliksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemiliks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('telepon')->nullable();
            $table->string('image')->default('default.jpg');
            $table->string('email')->unique();
            $table->string('password');
            $table->time('jam_buka')->nullable();
            $table->time('jam_tutup')->nullable();
            $table->longText('alamat')->nullable();
            $table->string('latlng')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemiliks');
    }
}
