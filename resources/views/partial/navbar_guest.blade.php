<nav class="navbar navbar-app">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="float-left navbar-toggle collapsed">
        <span class="icon-bar" style="background-color: #34495e "></span>
        <span class="icon-bar" style="background-color: #34495e "></span>
        <span class="icon-bar" style="background-color: #34495e "></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('img/logo_2.png')}}" style="height: 24px"></a>
    </div>
    <div class="side-collapse collapse navbar-collapse" id="collapse-menu">
      <ul class="nav navbar-nav">
        <li id="article" class="{{ request()->is('article') ? 'active' : '' }}">
          <a href="{{url('/article')}}">Artikel</a>
        </li>
        <li id="discount" class="{{ request()->is('discount') ? 'active' : '' }}">
          <a href="{{url('/discount')}}">Diskon</a>
        </li>
        <li id="salon" class="{{ request()->is('salon') ? 'active' : '' }}">
          <a href="{{url('/salon')}}">List Salon</a>
        </li>
        <li id="about" class="{{ request()->is('about') ? 'active' : '' }}">
          <a href="{{url('/about')}}">About</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <div class="input-group search-form">
            <a id="start_search"  class="input-group-btn cursor">
              <i class="material-icons">search</i>
            </a>
            <input type="text" class="form-control search-input" placeholder="Cari ..." id="search">
          </div>
        </li>
        <li>
          <a href="#" data-toggle="modal" data-target="#modalRegister">Daftar</a>
        </li>
        <li>
          <a href="#" data-toggle="modal" data-target="#modalLogin">Masuk</a>
        </li>
        <!-- <li><a href="{{url('/user/logout')}}">logout</a></li> -->
      </ul>
    </div>
  </div>
</nav>
<div class="back-drop hidden"></div>
<!-- Modal Login -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog col-login" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Login</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs border-0">
          <li class="active col-6 p-0 text-center">
            <a class="tabs-login" data-toggle="tab" href="#tabcustomer">Pengguna</a>
          </li>
          <li class=" col-6 p-0 text-center">
            <a class="tabs-login" data-toggle="tab" href="#tabsalon">Salon</a>
          </li>
        </ul>

        <div class="tab-content">
          <div id="tabcustomer" class="tab-pane fade in active">
           <form class="mt-3" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="m-0 mb-5 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <input placeholder="Email" id="emailcust" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="m-0 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <input placeholder="Password" id="passwordcust" type="password" class="form-control" name="password" required>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
            <div class="text-right mt-3">
              <a href="{{url('/register')}}">Belum punya akun ?</a>
            </div>
            <div class="mt-4">
              <button type="submit" class="btn btn-pink col-12">
                Login
              </button>
            </div>
          </form>  
        </div>
        <div id="tabsalon" class="tab-pane fade">
          <form class="mt-3" method="POST" action="{{ url('owner/login') }}">
            {{ csrf_field() }}
            <div class="m-0 mb-5 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <input placeholder="Email" id="emailowner" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="m-0 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <input placeholder="Password" id="passwordowner" type="password" class="form-control" name="password" required>
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
            <div class="text-right mt-3">
              <a href="{{url('/owner/register')}}">Belum punya akun ?</a>
            </div>
            <div class="mt-4">
              <button type="submit" class="btn btn-pink col-12">
                Login
              </button>
            </div>
          </form> 
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Modal Register -->
<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog col-register" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Daftar</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs border-0">
          <li class="active col-6 p-0 text-center">
            <a class="tabs-register" data-toggle="tab" href="#tabcustomerDaftar">Pengguna</a>
          </li>
          <li class=" col-6 p-0 text-center">
            <a class="tabs-register" data-toggle="tab" href="#tabsalonDaftar">Salon</a>
          </li>
        </ul>
        <div class="tab-content">
          <div id="tabcustomerDaftar" class="tab-pane fade in active">
            <form class="mt-3" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="m-0 mb-5 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input placeholder="Nama Pengguna" id="name2cust" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
              <div class="m-0 mb-5 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input placeholder="Email" id="email2cust" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
              <div class="m-0 mb-5 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input placeholder="Password" id="password2cust" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
              <div class="m-0 mb-5 form-group">
                <input placeholder="Konfirmasi Password" id="password-confirm2cust" type="password" class="form-control" name="password_confirmation" required>
              </div>
              <div class="text-right mt-3">
                <a href="{{url('/login')}}">Sudah punya akun ?</a>
              </div>
              <button type="submit" class="btn btn-pink col-12 mt-3">
                Daftar
              </button>
            </form>
          </div>


          <div id="tabsalonDaftar" class="tab-pane fade">
            <form class="mt-3" method="POST" action="{{ url('owner/register') }}">
              {{ csrf_field() }}
              <div class="m-0 mb-5 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <input placeholder="Nama Salon" id="name2owner" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
              </div>
              <div class="m-0 mb-5 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input placeholder="Email" id="email2owner" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
              <div class="m-0 mb-5 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input placeholder="Password" id="password2owner" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
              </div>
              <div class="m-0 mb-5 form-group">
                <input placeholder="Konfirmasi Password" id="password-confirm2owner" type="password" class="form-control" name="password_confirmation" required>
              </div>
              <div class="text-right mt-3">
                <a href="{{url('/owner/login')}}">Sudah punya akun ?</a>
              </div>
              <button type="submit" class="btn btn-pink col-12 mt-3">
                Daftar
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#start_search').click(function () {
    location.href="/search/"+$('#search').val();
      console.log($('#search').val())
  })

  $('.collapsed,.back-drop').click(function () {
    $('.side-collapse').toggleClass('on');
    $('.back-drop').toggleClass('hidden');
  })

    var options = {
  //data: ["blue", "green", "pink", "red", "yellow"],
  url:'/uniq',
  list: {
      match: {
        enabled: true
      }
    }
};

$("#search").easyAutocomplete(options);
</script>