<nav id="sidebar">
  <div class="sidebar-header text-center">
    <img src="{{asset('img/logo.png')}}" class="img-profile rounded-0" style="height: 90%;width: 90%">
    <div class="text-center">
    {{Auth::user()->name}}
    </div>
  </div>
  <ul class="list-unstyled components">
    <li id="adm-dashboard" class="{{ request()->is('admin/index') ? 'active' : '' }}">
      <a href="{{url('/admin/index')}}">
        <i class="material-icons">dashboard</i>Dashboard
      </a>
    </li>
     <li id="adm-owner" class="{{ request()->is('admin/owner') ? 'active' : '' }}">
      <a href="{{url('/admin/owner')}}">
        <i class="material-icons">supervisor_account</i>Owner Salon
      </a>
    </li>
     <li id="adm-user" class="{{ request()->is('admin/user') ? 'active' : '' }}">
      <a href="{{url('/admin/user')}}">
        <i class="material-icons">person</i>User
      </a>
    </li>
     <li id="adm-slider" class="{{ request()->is('admin/slider') ? 'active' : '' }}">
      <a href="{{url('/admin/slider')}}">
        <i class="material-icons">view_carousel</i>Slider
      </a>
    </li>
     <li id="adm-article" class="{{ request()->is('admin/article') ? 'active' : '' }}">
      <a href="{{url('/admin/article')}}">
        <i class="material-icons">style</i>Artikel
      </a>
    </li>
     <li id="adm-video" class="{{ request()->is('admin/video') ? 'active' : '' }}">
      <a href="{{url('/admin/video')}}">
        <i class="material-icons">movie</i>Video
      </a>
    </li>
     <li id="adm-rekening" class="{{ request()->is('admin/rekening') ? 'active' : '' }}">
      <a href="{{url('/admin/rekening')}}">
        <i class="material-icons">money</i>Nomor Rekening
      </a>
    </li>
    <li id="adm-about" class="{{ request()->is('admin/about') ? 'active' : '' }}">
      <a href="{{url('/admin/about')}}">
        <i class="material-icons">info</i>About
      </a>
    </li>
    <li id="adm-order">
      <a id="adm-menu" href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
        <i class="material-icons">payment</i>
        Pesanan
      </a>
      <ul class="collapse list-unstyled" id="pageSubmenu">
        <li id="adm-order-bb"><a href="{{url('admin/order/belum-bayar')}}">Belum Bayar</a></li>
        <li id="adm-order-tb"><a href="{{url('admin/order/telah-bayar')}}">Telah Bayar</a></li>
        <li id="adm-order-td"><a href="{{url('admin/order/telah-diterima')}}">Telah Diterima</a></li>
        <li id="adm-order-s"><a href="{{url('admin/order/selesai')}}">Selesai</a></li>
        <li id="adm-order-b"><a href="{{url('admin/order/batal')}}">Batal</a></li>
      </ul>
    </li>
  </ul>
</nav>