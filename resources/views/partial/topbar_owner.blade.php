<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header" style="min-width: 30rem;">
      <div class="d-inline-block" style="min-width: 190px">
      <span class="text-bold font-18 mr-3">Owner Salon</span>
      </div>
      <i class="material-icons cursor" id="sidebarCollapse">menu</i>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/owner/logout')}}">logout</a></li>
      </ul>
    </div>
  </div>
</nav>