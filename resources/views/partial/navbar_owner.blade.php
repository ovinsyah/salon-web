<nav class="navbar navbar-app">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="float-left navbar-toggle collapsed">
        <span class="icon-bar" style="background-color: #34495e "></span>
        <span class="icon-bar" style="background-color: #34495e "></span>
        <span class="icon-bar" style="background-color: #34495e "></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('img/logo_2.png')}}" style="height: 24px"></a>
    </div>
    <div class="side-collapse collapse navbar-collapse" id="collapse-menu">
      <ul class="nav navbar-nav mt-0">
        <li class="m-profile">
          <a href="{{url('/owner/profile')}}">
            <img src="{{asset('images/owner')}}/{{Auth::guard('pemilik')->user()->image}}" class="m-profile-img">
            <span class="ml-3 text-bold font-18">{{Auth::guard('pemilik')->user()->name}}</span>
          </a>
        </li>
        <li id="article" class="{{ request()->is('article') ? 'active' : '' }}">
          <a href="{{url('/article')}}">Artikel</a>
        </li>
        <li id="discount" class="{{ request()->is('discount') ? 'active' : '' }}">
          <a href="{{url('/discount')}}">Diskon</a>
        </li>
        <li id="salon" class="{{ request()->is('salon') ? 'active' : '' }}">
          <a href="{{url('/salon')}}">List Salon</a>
        </li>
        <li id="about" class="{{ request()->is('about') ? 'active' : '' }}">
          <a href="{{url('/about')}}">About</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <div class="input-group search-form">
            <a href="{{url('/search')}}" class="input-group-btn cursor">
              <i class="material-icons">search</i>
            </a>
            <input type="text" class="form-control search-input" placeholder="Cari ...">
          </div>
        </li>
        <li class="m-logout">
          <a href="{{url('/owner/logout')}}">Logout</a>
        </li>
        <li class="dropdown ml-2 account">
          <a href="#" class="dropdown-toggle p-0 pt-2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{asset('images/owner')}}/{{Auth::guard('pemilik')->user()->image}}" class="nav-profile">
          </a>
          <ul class="dropdown-menu">
            <li><a href="{{url('/owner/profile')}}">Profile</a></li>
            <li><a href="{{url('/owner/service-salon')}}">Salon</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{url('/owner/logout')}}">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="back-drop hidden"></div>
<script type="text/javascript">
  $('.collapsed,.back-drop').click(function () {
    $('.side-collapse').toggleClass('on');
    $('.back-drop').toggleClass('hidden');
  })
</script>