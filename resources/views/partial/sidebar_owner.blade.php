<nav id="sidebar">
  <div class="sidebar-header text-center">
    <img src="{{asset('img/salon_logo.jpg')}}" class="img-profile">
    <div class="sidebar-name-profile">
      <span>Sir Salon Medan</span>
    </div>
  </div>
  <ul class="list-unstyled components">
    <li id="adm-dashboard" class="{{ request()->is('admin/index') ? 'active' : '' }}">
      <a href="{{url('/admin/index')}}">
        <i class="material-icons">dashboard</i>Dashboard
      </a>
    </li>
     <li id="adm-owner" class="{{ request()->is('admin/owner') ? 'active' : '' }}">
      <a href="{{url('/admin/owner')}}">
        <i class="material-icons">card_travel</i>Layanan
      </a>
    </li>
     <li id="adm-user" class="{{ request()->is('admin/user') ? 'active' : '' }}">
      <a href="{{url('/admin/user')}}">
        <i class="material-icons">location_on</i>Lokasi
      </a>
    </li>
    <li id="adm-order">
      <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
        <i class="material-icons">payment</i>
        Pesanan
      </a>
      <ul class="collapse list-unstyled" id="pageSubmenu">
        <li id="adm-order-bb"><a href="{{url('admin/order/belum-bayar')}}">Belum Bayar</a></li>
        <li id="adm-order-tb"><a href="{{url('admin/order/telah-bayar')}}">Telah Bayar</a></li>
        <li id="adm-order-s"><a href="{{url('admin/order/selesai')}}">Selesai</a></li>
      </ul>
    </li>
  </ul>
</nav>