@extends('master.master_main')
@section('content')
<div class="bg-gray">
  <div class="col-service mb-5">
    <div class="row" style="margin: -8px">
      @foreach($services as $service)
      <div class="col-sm-6 col-md-4 p-3">
      <a href="{{url('detail/service',$service->id)}}">
        <div class="thumb-service">
          <div class="thumb-service-img">
           <img src="{{asset('images/service')}}/{{$service->sampul}}">
         </div>
         <div class="thumb-service-disc">
          {{$service->diskon}}%
         </div>
         <div class="thumb-service-body">
          <div class="thumb-service-price">
            <span class="price-old mr-3">Rp {{number_format($service->harga,0,',','.')}} </span><span>Rp {{number_format($service->harga-($service->diskon*$service->harga)/100,0,',','.')}}</span>         
          </div>
         <div class="row m-0">
           <div class="col pl-0 mt-2 thumb-service-title">
             <span>{{$service->deskripsi}} </span>
           </div>
           <div class="col p-0 mt-2" style="max-width: 45px">
             <img src="{{asset('images/owner')}}/{{$service->pemilik->image}}">
           </div>
         </div>
         <div class="mt-2">
          <a href="{{url('detail/salon',$service->pemilik->id)}}">
           <i class="material-icons mr-2">store</i><span>{{$service->pemilik->name}}</span>
          </a>
         </div>
       </div>
     </div>
   </a>
   </div>
     @endforeach
   </div>
</div>
</div>
@endsection