@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row">
    @foreach($articles as $article)
    <div class="col-md-6 mb-3">
      <div class="thumb-news">
      <div class="row m-0">
        <div class="col thumb-news-img mt-2 mb-2 p-0">
          <img src="{{asset('images/article')}}/{{$article->sampul}}">
        </div>
        <div class="col">
          <div class="thumb-news-title">{{$article->judul}}</div>
          <div class="thumb-news-desc">
            {!!$article->deskripsi!!}
          </div>
        </div>
      </div>
          <div class="read-news2">
            <a href="{{url('detail/article',$article->id)}}" class="btn btn-pink">Baca Artikel</a>
          </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection