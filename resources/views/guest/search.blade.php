@extends('master.master_main')
@section('content')
<div class="bg-gray">
  <div class="col-service mb-5">
    <div class="font-18 mb-3">Pencarian : <i> {{$key}} </i></div>
    <div class="row" style="margin: -8px">
      @foreach($services as $service)
            <div class="col-sm-6 col-md-4 p-3">
        <a href="{{url('detail/service',$service->id)}}">
          <div class="thumb-service">
            <div class="thumb-service-img">
             <img src="{{asset('images/service')}}/{{$service->sampul}}">
           </div>
           @if($service->diskon != 0)
           <div class="thumb-service-disc">
            {{$service->diskon}}%
          </div>
          @endif
          <div class="thumb-service-body">
            <div class="thumb-service-price">
              @if($service->diskon != 0)
              <span class="price-old mr-3">Rp{{$service->harga}}</span>
              @endif
              <span>Rp {{$service->harga-($service->diskon*$service->harga)/100}}</span>
            </div>
            <div class="row m-0">
             <div class="col pl-0 mt-2 thumb-service-title">
               <span>{{$service->deskripsi}}</span>
             </div>
             <div class="col p-0 mt-2" style="max-width: 45px">
               <img src="{{asset('images/owner')}}/{{$service->pemilik->image}}">
             </div>
           </div>
           <div class="mt-2">
            <a href="{{url('detail/salon',$service->pemilik->id)}}">
             <i class="material-icons mr-2">store</i><span>{{$service->pemilik->name}}</span>
           </a>
         </div>
       </div>
     </div>
   </a>
 </div>
     @endforeach
   </div>
</div>
</div>
@endsection