@extends('master.master_main')
@section('content')
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
      @foreach($sliders as $i=>$slider)
        <li data-target="#myCarousel" data-slide-to="{{$i}}" class="active"></li>
      @endforeach
  </ol>
  <div class="carousel-inner">
    @foreach($sliders as $key => $slider)
    <div id="itemSlider{{$key}}" class="item text-center">
      <a href="{{url('detail/slider',$slider->id)}}">
      <img src="{{asset('images/slider')}}/{{$slider->sampul}}" class="img-carousel">
      </a>
    </div>
    @endforeach
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div class="col-home">
  <div class="col-news mb-5">
  	<div class="text-bold font-22 text-center">Artikel Terbaru</div>
  	<div class="row" style="margin: -8px">
  		@foreach($articles as $article)
  		<div class="col-sm-6 col-md-3 p-3">
  			<div class="thumb-news">
  				<div class="thumb-news-img">
           <img src="{{asset('images/article')}}/{{$article->sampul}}">
         </div>
         <div class="thumb-news-title">
           <span>{{$article->judul}}</span>
         </div>
         <div class="thumb-news-desc">{!!$article->deskripsi!!}</div>
         <div class="read-news">
           <a href="{{url('detail/article',$article->id)}}" class="btn btn-pink">Baca Artikel</a>
         </div>
       </div>
     </div>
     @endforeach
   </div>
 </div>
 <div class="col-news mb-5">
   <div class="text-bold font-22 text-center">Video</div>
   <div class="row" style="margin: -8px">
    @foreach($videos as $video)
    <div class="col-sm-6 col-md-3 p-3">
     <video controls class="thumb-video">
       <source src="{{asset('video')}}/{{$video->video}}" type="video/mp4">
       </video>
       <span class="thumb-video-title">{{$video->judul}} </span>
     </div>
     @endforeach
   </div>
 </div>
</div>
<div class="bg-gray">
  <div class="col-service mb-5">
    <div class="text-bold font-18">Layanan Salon Terbaru</div>
    <div class="row" style="margin: -8px">
      @foreach($services as $service)
      <div class="col-sm-6 col-md-4 p-3">
        <a href="{{url('detail/service',$service->id)}}">
          <div class="thumb-service">
            <div class="thumb-service-img">
             <img src="{{asset('images/service')}}/{{$service->sampul}}">
           </div>
           @if($service->diskon != 0)
           <div class="thumb-service-disc">
            {{$service->diskon}}%
          </div>
          @endif
          <div class="thumb-service-body">
            <div class="thumb-service-price">
              @if($service->diskon != 0)
              <span class="price-old mr-3">Rp {{number_format($service->harga,0,',','.')}}</span>
              @endif
              <span>Rp {{number_format($service->harga-($service->diskon*$service->harga)/100,0,',','.')}}</span>
            </div>
            <div class="row m-0">
             <div class="col pl-0 mt-2 thumb-service-title">
               <span>{{$service->deskripsi}}</span>
             </div>
             <div class="col p-0 mt-2" style="max-width: 45px">
               <img src="{{asset('images/owner')}}/{{$service->pemilik->image}}">
             </div>
           </div>
           <div class="mt-2">
            <a href="{{url('detail/salon',$service->pemilik->id)}}">
             <i class="material-icons mr-2">store</i><span>{{$service->pemilik->name}}</span>
           </a>
         </div>
       </div>
     </div>
   </a>
 </div>
 @endforeach
</div>
</div>
</div>
<script type="text/javascript">
  $('#itemSlider0').addClass('active');
</script>
@endsection