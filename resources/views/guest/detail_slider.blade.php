@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="text-center">
    <img src="{{asset('images/slider')}}/{{$slider->sampul}}" class="img-article">
  </div>
  <div class="thumb-news-title mt-2">
    <span class="font-18">{{$slider->judul}}</span>
  </div>
  <div class="font-16 mt-3">
   {!!  $slider->deskripsi !!}
  </div>
</div>
@endsection