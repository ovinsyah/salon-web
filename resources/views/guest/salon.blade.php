@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
    <div class="row" style="margin: -8px">
      @foreach($salons as $salon)
      <div class="col-6 col-md-3 p-3">
        <div class="thumb-salon p-3 bg-thumb-salon">
          <div class="thumb-salon-img">
           <img src="{{asset('images/owner')}}/{{$salon->image}}">
         </div>
         <div class="thumb-salon-title">
           <span>{{$salon->name}}</span>
         </div>
         <div class="thumb-salon-addr">
           <span>{{$salon->alamat}}</span>
         </div>
         <div class="thumb-salon-open">
            <span class="open-salon">Buka</span>
            <span>{{$salon->jam_buka}} - {{$salon->jam_tutup}}</span>
         </div>
         <div class="read-salon">
           <a href="{{url('detail/salon',$salon->id)}}" class="btn btn-pink">Lihat detail</a>
         </div>
       </div>
     </div>
     @endforeach
   </div>
</div>
@endsection