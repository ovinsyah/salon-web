@extends('master.master_main')
@section('content')
<div class="col-home mt-5 mb-5">
  <div class="row m-0">

    <div class="col-md-7 p-0">
      <div class="salon-img">
       <img src="{{asset('images/owner')}}/{{$salon->image}}">
     </div>
     <div class="thumb-salon-title mt-3">
       <span>{{$salon->name}}</span>
     </div>
     <div class="thumb-salon-addr">
       <span>{{$salon->alamat}}</span>
     </div>
     <div class="row m-0 mt-3">
       <div class="col p-0">
         <i class="material-icons">phone</i>
         <span class="ml-2 text-bold">{{$salon->telepon}}</span>
       </div>
       <div class="col p-0 text-right thumb-salon-open">
        <span class="open-salon">Buka</span>
        <span>{{$salon->jam_buka}} - {{$salon->jam_tutup}}</span>
      </div>
    </div>
    <div class="text-bold font-16 mt-5">
      <span>Lokasi</span>
    </div>
    {{$salon->alamat}}
    <div id="map" class="mb-5"></div>
  </div>

  <!--   service -->
  <div class="col-md-5 pr-0" id="service_salon">
    <div class="text-bold font-16 mb-3">
      <span>Layanan Salon</span>
    </div>
    <div class="col-service-salon">
      @foreach($salon->service as $service)
      <div class="row m-0 mb-3">
        <div class="col p-0 text-center" style="max-width: 90px">
          <img src="{{asset('images/service')}}/{{$service->sampul}}" class="img-menu-service">
          <input type="hidden" value="{{asset('images/service')}}/{{$service->sampul}}" name="" id="gbr{{$service->id}}">
        </div>
        <div class="col">
          <a href="{{url('detail/service/1')}}">
            <div class="title-menu-service">
              {{$service->nama}}
            </div>
            @if($service->diskon != 0)
            <span class="price-old mr-3">Rp {{number_format($service->harga,0,',','.')}}</span>
            @endif
            <span class="">Rp. {{$service->harga-($service->diskon*$service->harga)/100}}</span>
            <div class="desc-menu-service">
              {{$service->deskripsi}}
              <input type="hidden" name="" value="{{$service->deskripsi}}" id="desc{{$service->id}}">
            </div>
          </a>
        </div>
        <div class="col p-0 mt-5" style="max-width: 90px">
          <button class="btn btn-number min">-</button>
          <input datanama="{{$service->nama}}" dataprice="{{$service->harga-($service->diskon*$service->harga)/100}}" dataid="{{$service->id}}" type="number" name="" value="0" class="numb sum_check" style="width: 35px">
          <button class="btn btn-number add">+</button>
        </div>
      </div>
      @endforeach
    </div>
    <div class="form-checkout font-18">
      <div class="form-checkout-bill text-bold">
        <div class="row m-0">
          <div class="col p-0">Total harga pesanan</div>
          <div class="col p-0 text-right"><span id="price">Rp. 0</span></div>
        </div>
      </div>
      <div class="text-center mt-5">
         @if (Auth::guard('pemilik')->check()) 
            <span>Owner Salon Tidak Dapat Checkout</span>
        @elseif (Auth::guard('web')->check()) 
             <button class="btn btn-pink disabled" id="checkout" style="width: 20rem" >Pesan</button>
        @else
             <button class="btn btn-pink" data-toggle="modal" data-target="#modalLogin" style="width: 20rem">Pesan</button>
        @endif
     </div>
   </div>
 </div>
 <!--   Endservice -->

</div>
<label>Ulasan</label>
@foreach($salon->service as $service)
  @foreach($service->ulasan as $ulasan)
  <div class="row m-0 mb-3" style="border-bottom: 1px solid #c5c5c5;padding-bottom:  3px;">
    <div class="col p-0 text-center" style="max-width: 10rem">
      <img src="{{asset('images/user')}}/{{$ulasan->user->image}}">
      <span class="text-bold">{{$ulasan->user->name}}</span>
    </div>
    <div class="col pr-0">
      <div>
        @for($i=0;$i<$ulasan->rating;$i++) 
        <span class="glyphicon rating active font-24 glyphicon-star"></span>
        @endfor
        <a href="{{url('detail/service',$ulasan->service->id)}}" class="text-bold font-16" style="position:  relative;top: -5px;">{{$ulasan->service->nama}}</a>
      </div>
      <div>
          {!!$ulasan->comment!!}
      </div>
    </div>
  </div>
  @endforeach
@endforeach
</div>
<!-- Modal -->
<div class="modal fade" id="modalDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tanggal Booking</h4>
      </div>
      <div class="modal-body">
        <div id="view_check"></div>
        <div class="text-right" style="border-top: 1px solid #e4e4e4;padding-top:  6px;">
          <span id="total_price" class="font-18 text-bold"></span>
        </div>
        <div class="row m-0 mt-4">
          <div class="col pt-2 text-right">
            Jadwal Datang
          </div>
          <div class="col p-0" style="max-width: 20rem">
            <div class="form-group">
              <div class='input-group date' id='datetimepicker1'>
                <input type='text' class="disabled form-control" id="time_new" />
                <span class="input-group-addon" id="time_set">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="row m-0">
          <div class="col pt-2 text-right">
            Nomor HP
          </div>
          <div class="col p-0" style="max-width: 20rem">
            @if (Auth::guard('web')->check()) 
            <input type="" name="" value="{{Auth::user()->telepon}}" class="form-control" id="noPhone">
            @else
            @endif
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-pink disabled" id="create_order">Buat Pesanan</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="" value="{{$salon->latlng}}" id="latlng">

<script>
  /*  Main Function*/
  var listArr = [];

  var nowDate = new Date();
  var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
  $('#datetimepicker1').datetimepicker({
    format: 'DD-MM-YY HH:mm',
    minDate:today
  });
  $('#service_salon').on('click change keyup',function () {
    var total = 0;
    $('.sum_check').each(function () {
      var jumlah = $(this).val();
      var harga = ($(this).attr('dataprice'));
      total += (parseInt(jumlah)*parseInt(harga));
    });
    $('#price').text(convertToRupiah(total));
    $('#total_price').text(convertToRupiah(total));

    //cek button pesan
    $('#checkout').addClass('disabled');
    $('.sum_check').each(function () {
      var jumlah = $(this).val();
      if(jumlah != 0){
        $('#checkout').removeClass('disabled');
      }
    });
  });

  $('#checkout').click(function () {
    $('#modalDate').modal('toggle');
    listArr = []
    $('#view_check').empty();
    $('.sum_check').each(function () {
      var id = ($(this).attr('dataid'));
      var jumlah = $(this).val();
      var harga = ($(this).attr('dataprice'));
      var nama = ($(this).attr('datanama'));
      var desc = $('#desc'+id).val();
      var gbr = $('#gbr'+id).val();
      if(jumlah != 0){
        listArr.push({
          'id':id,
          'jumlah':jumlah,
          'harga':harga,
          'nama':nama,
          'desc':desc,
          'gbr':gbr
        });
      }
    });
    for(i=0; i< listArr.length; i++){
      $('#view_check').append(`
        <div class="row m-0 mb-3">
        <div class="col p-0 text-center" style="max-width: 90px">
        <img src="`+listArr[i].gbr+`" class="img-menu-service">
        </div>
        <div class="col">
        <div class="title-menu-service">
        `+listArr[i].nama+`
        </div>
        <span class="">`+convertToRupiah(listArr[i].harga)+`</span>
        <div class="desc-menu-service">
        `+listArr[i].desc+`
        </div>
        </div>
        <div class="col p-0 mt-5 text-center" style="max-width: 90px">
        x`+listArr[i].jumlah+`</br>
        `+convertToRupiah(parseInt(listArr[i].harga)*parseInt(listArr[i].jumlah))+`
        </div>
        </div>
        `);
    }
  });
  $('#time_set').click(function () {
    $('#create_order').removeClass('disabled');
  })
  $('#create_order').click(function () {
    console.log(listArr);
    console.log($('#time_new').val());
    kirim();
  });

   //cek create order
  var statusTime = 0;
  $('#time_set').click(function () {
    statusTime = 1;
  })
  $('#formOrder').on('change keyup click',function () {
    $('#create_order').addClass('disabled');
    if($('#noPhone').val() != '' && statusTime == 1){
      $('#create_order').removeClass('disabled');
    }
  });

  /*  Render Lokasi Maps*/
  function initMap() {
    var tmplatlng = $('#latlng').val();
    var latlng = tmplatlng.split(",");
    var x = parseFloat(latlng[0]);
    var y = parseFloat(latlng[1]);
    var myLatLng = {lat: x, lng: y};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: myLatLng
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Hello World!'
    });
  }

  function kirim() {

    listArr.push({
      jam:$('#time_new').val(),
      user:"{{(Auth::guard('web')->check())?Auth::guard('web')->user()->id:0}}",
      pemilik:"{{$salon->id}}",
      telepon: $('#noPhone').val()
    });
    $.ajax({
      url:"/api/user/create/order",
      type:"POST",
      data:JSON.stringify(listArr),
      dataType: 'json',
      contentType: 'application/json',
      success:function (data) {
       location.href="/user/order";
       console.log(data);
     }
   })  
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection