@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="text-center">
    <img src="{{asset('images/article')}}/{{$article->sampul}}" class="img-article">
  </div>
  <div class="thumb-news-title mt-2">
    <span class="font-18">{{$article->judul}}</span>
  </div>
  <div class="font-16 mt-3">
    {!!$article->deskripsi!!}
  </div>
</div>
<script type="text/javascript">
  $('#article').addClass('active');
</script>
@endsection