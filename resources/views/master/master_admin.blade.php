<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Salon</title>
    <meta name="description" content="">
    <meta name="author" content="">
	
</head>
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-grid.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/import.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/admin.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/material-icons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/summernote.css')}}">

<link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('js/summernote.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>

<body>
		@include('partial.topbar_admin')<!-- {{Carbon\Carbon::now()->toDateString()}}|{{Carbon\Carbon::parse(App\Antrian::first()->updated_at)->toDateString()}} -->
		<!-- <a href="">{{(Carbon\Carbon::now()->toDateString()==Carbon\Carbon::parse(App\Antrian::first()->updated_at)->toDateString())?'true':'false'}}</a> -->
	<div class="wrapper">
		@include('partial.sidebar_admin')
		<div id="content">
			@yield('content')
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function () {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
		});
	});
</script>
</html>