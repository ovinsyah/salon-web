@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/user')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/user/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold pink">Profil Pengguna</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/user/order')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Pesanan</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
       @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
        </div>
      @elseif ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
        </div>
      @endif
      <div class="row m-0 pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <div class="col p-0">
          <span class="text-bold font-20">Profile Pengguna</span>
        </div>
        <div class="col p-0 text-right">
          <a href="{{url('/user/edit/profile')}}" class="btn btn-app pt-1 pb-1 font-16"><i class="material-icons">edit</i> Edit</a>
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Nama Pengguna</div>
        <div class="col p-0 font-16">{{$user->name}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Email</div>
        <div class="col p-0 font-16">{{$user->email}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Telepon</div>
        <div class="col p-0 font-16">{{$user->telepon}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Profile Pengguna</div>
        <div class="col p-0">
          <img src="{{asset('images/user')}}/{{$user->image}}" class="profile-owner-upload">
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Password</div>
        <div class="col p-0 font-16 text-bold">
          <span>*********</span><a class="font-14 ml-3" href="{{url('/user/change-password')}}" style="position:  relative;top: -4px;color: #0e6fb7;">Ganti</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection