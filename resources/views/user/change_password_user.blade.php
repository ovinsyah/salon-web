@extends('master.master_main')
@section('content')
<div class="col-home mt-5" id="formApp">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/user')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/user/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold pink">Profil Pengguna</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/user/order')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Pesanan</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
        </div>
      @endif
      <div class="pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-20">Ganti Password</span>
      </div>
     <form method="post" action="{{url('user/update/password')}}" enctype="multipart/form-data">
            {{csrf_field()}} 
      <div class="row m-0 mt-4">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Password Lama</div>
        <div class="col p-0 font-16">
          <input type="password" name="old" class="form-control" style="width: 30rem" required>
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Password Baru</div>
        <div class="col p-0 font-16">
          <input type="password" name="new" id="pass" class="form-control" style="width: 30rem" required>
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Konfirmasi Password</div>
        <div class="col p-0 font-16">
          <input type="password" name="reType" id="repass" class="form-control" style="width: 30rem" required>
        </div>
      </div>
      <div class="text-right">
        <button  type="submit" class="btn btn-app" id="save">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#formApp').on('click change keyup',function () {
    $('#pass,#repass').removeClass('form-error');
    $('#save').removeClass('disabled');
    if($('#pass').val() != $('#repass').val()){
      $('#pass,#repass').addClass('form-error');
      $('#save').addClass('disabled');
    }
  });
</script>
@endsection