@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/user')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
        <input value="{{Auth::user()->name}}" type="hidden" name="" id="username">
      </div>
      <div class="mt-5">
        <a href="{{url('/user/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold">Profil Pengguna</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/user/order')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold pink">Pesanan</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2 mb-4" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-20">Pesanan Pengguna</span>
      </div>
      <ul class="nav nav-tabs border-0 row m-0">
        <li class="active col p-0 text-center">
          <a class="tabs-login pl-1 pr-1" data-toggle="tab" href="#belumbayar">Belum Bayar</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login pl-1 pr-1" data-toggle="tab" href="#telahbayar">Telah Bayar</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login pl-1 pr-1" data-toggle="tab" href="#telahditerima">Telah Diterima</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login pl-1 pr-1" data-toggle="tab" href="#selesai">Selesai</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login pl-1 pr-1" data-toggle="tab" href="#batal">Batal</a>
        </li>
      </ul>

      <div class="tab-content">
        <div id="belumbayar" class="tab-pane fade in active">
          @foreach($orders as $order)
          @if($order->status==0)
          <div class="row m-0 list-order">
            <div class="col p-0 user-order-img">
             <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
           </div>
           <div class="col p-0">
            <?php
            $total = 0;
            ?>
            @foreach($order->list as $item)
            <?php
            $total += $item->qty*$item->harga;
            ?>
            <div class="row m-0 mb-2">
              <div class="col p-0 ellipsis-1">
                {{$item->layanan->nama}}
              </div>
              <div class="col p-0 text-center">
                x{{$item->qty}}
              </div>
              <div class="col p-0 text-right">
                Rp {{number_format($item->harga,0,',','.')}}
              </div>
            </div>
            @endforeach
            <div class="col-12 row m-0 p-0 list-order-total">
              <div class="col p-0">
                <a href="">{{$order->pemilik->name}}</a>
              </div>
              <div class="col p-0 text-right">
                Total : Rp {{number_format($total,0,',','.')}}<br>
                <span  class="text-thin">
                  Kedatangan : {{$order->jam_datang}}
                </span>
              </div>
            </div>
          </div>
          <div class="col-12 p-0 mt-2">
            <button dataid="{{$order->id}}" class="btn btn-danger pt-1 pb-1 batal" data-toggle="modal" data-target="#modalDelete">Batal</button>
            <button dataid="{{$order->id}}" datatotal="{{$total}}" class="btn btn-success pt-1 pb-1 bayar" data-toggle="modal" data-target="#modalBayar">Bayar</button>
          </div>
        </div>
        @endif
        @endforeach
      </div>
      <div id="telahbayar" class="tab-pane fade">
        @foreach($orders as $order)
        @if($order->status==1 )
        <div class="row m-0 list-order">
          <div class="col p-0 user-order-img">
            <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
          </div>
          <div class="col p-0">
            <?php
            $total = 0;
            ?>
            @foreach($order->list as $item)
            <?php
            $total += $item->qty*$item->harga;
            ?>
            <div class="row m-0 mb-2">
              <div class="col p-0 ellipsis-1">
                {{$item->layanan->nama}}
              </div>
              <div class="col p-0 text-center">
                x{{$item->qty}}
              </div>
              <div class="col p-0 text-right">
                Rp {{number_format($item->harga,0,',','.')}}
              </div>
            </div>
            @endforeach
            <div class="col-12 row m-0 p-0 list-order-total">
              <div class="col p-0">
                <a href="">{{$order->pemilik->name}}</a>
              </div>
              <div class="col p-0 text-right">
                Total : Rp {{number_format($total,0,',','.')}}<br>
                <span  class="text-thin">
                  Kedatangan : {{$order->jam_datang}}
                </span>
              </div>
            </div>
          </div>
        </div>
        @endif
        @endforeach
      </div>
      <div id="telahditerima" class="tab-pane fade">
        @foreach($orders as $order)
        @if($order->status==2)
        <div class="row m-0 list-order">
          <div class="col p-0 user-order-img">
            <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
          </div>
          <div class="col p-0">
            <?php
            $total = 0;
            ?>
            @foreach($order->list as $item)
            <?php
            $total += $item->qty*$item->harga;
            ?>
            <div class="row m-0 mb-2">
              <div class="col p-0 ellipsis-1">
                {{$item->layanan->nama}}
                <input type="hidden" name="" value="{{$item->layanan->nama}}" class="nama{{$order->id}}">
              </div>
              <div class="col p-0 text-center">
                x{{$item->qty}}
                <input type="hidden" name="" value="{{$item->qty}}" class="qty{{$order->id}}">
              </div>
              <div class="col p-0 text-right">
                Rp {{number_format($item->harga,0,',','.')}}
              </div>
            </div>
            @endforeach
            <div class="col-12 row m-0 p-0 list-order-total">
              <div class="col p-0">
                <a href="">{{$order->pemilik->name}}</a>
              </div>
              <div class="col p-0 text-right">
                Total : Rp {{number_format($total,0,',','.')}}<br>
                <span  class="text-thin">
                  Kedatangan : {{$order->jam_datang}}
                </span>
              </div>
            </div>
          </div>
          <div class="col-12 p-0 mt-2">
            <button datasalon="{{$order->pemilik->name}}" dataantri="{{$order->no_antrian}}" dataid="{{$order->id}}" datajam="{{$order->jam_datang}}" datainvoice="{{$order->invoice}}" dataidorder="{{$order->id}}" class="btn btn-danger pt-1 pb-1 invoice" data-toggle="modal" data-target="#modalInvoice">Invoice</button>
            <button dataid="{{$order->id}}" class="btn btn-success pt-1 pb-1 verif" data-toggle="modal" data-target="#modalVerif">Verifikasi</button>
          </div>
        </div>
        @endif
        @endforeach
      </div>
      <div id="selesai" class="tab-pane fade">
        @foreach($orders as $order)
        @if($order->status==3)
        <div class="row m-0 list-order">
          <div class="col p-0 user-order-img">
            <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
          </div>
          <div class="col p-0">
            <?php
            $total = 0;
            ?>
            @foreach($order->list as $item)
            <?php
            $total += $item->qty*$item->harga;
            ?>

            <div class="row m-0 mb-2">
              <div class="col p-0 pt-1 ellipsis-1">
                {{$item->layanan->nama}}
                <input type="hidden" name="" value="{{$item->layanan->nama}}" class="nama{{$order->id}}">
              </div>
              <div class="col p-0 pt-1 text-center">
                x{{$item->qty}}
                 <input type="hidden" name="" value="{{$item->qty}}" class="qty{{$order->id}}">
              </div>
              <div class="col p-0 pt-1">
                Rp {{number_format($item->harga,0,',','.')}}
              </div>
              <div class="col p-0 text-right">
                @if((count(App\Ulasan::where('user_id',Auth::user()->id)->where('layanan_id',$item->layanan->id)->get())==0))
                <button dataid="{{$item->layanan->id}}" class="btn btn-success pr-2 pl-2 pt-1 pb-1 nilai" data-toggle="modal" data-target="#modalRating"  style="width: 60px;">Nilai</button>
                @else
                <a href="{{url('detail/service',$item->layanan->id)}}" class="btn pt-1 pb-1 btn-info" style="width: 60px;">Lihat</a>
                @endif
              </div>
            </div>
            @endforeach
            <div class="col-12 row m-0 p-0 list-order-total">
              <div class="col p-0">
                <a href="">{{$order->pemilik->name}}</a>
              </div>
              <div class="col p-0 text-right">
                Total : Rp {{number_format($total,0,',','.')}}<br>
                <span  class="text-thin">
                  Kedatangan : {{$order->jam_datang}}
                </span>
              </div>
            </div>
          </div>
          <div class="col-12 p-0">
            <button datasalon="{{$order->pemilik->name}}" dataantri="{{$order->no_antrian}}" dataid="{{$order->id}}" datajam="{{$order->jam_datang}}" datainvoice="{{$order->invoice}}" dataidorder="{{$order->id}}" class="btn btn-danger pt-1 pb-1 invoice" data-toggle="modal" data-target="#modalInvoice">Invoice</button>
          </div>
        </div>
        @endif
        @endforeach
      </div>
      <div id="batal" class="tab-pane fade">
        @foreach($orders as $order)
        @if($order->status==4)
        <div class="row m-0 list-order">
          <div class="col p-0 user-order-img">
            <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
          </div>
          <div class="col p-0">
            <?php
            $total = 0;
            ?>
            @foreach($order->list as $item)
            <?php
            $total += $item->qty*$item->harga;
            ?>
            <div class="row m-0 mb-2">
              <div class="col p-0 ellipsis-1">
                {{$item->layanan->nama}}
              </div>
              <div class="col p-0 text-center">
                x{{$item->qty}}
              </div>
              <div class="col p-0 text-right">
                Rp {{number_format($item->harga,0,',','.')}}
              </div>
            </div>
            @endforeach
            <div class="col-12 row m-0 p-0 list-order-total">
              <div class="col p-0">
                <a href="">{{$order->pemilik->name}}</a>
              </div>
              <div class="col p-0 text-right">
                Total : Rp {{number_format($total,0,',','.')}}<br>
                <span  class="text-thin">
                  Kedatangan : {{$order->jam_datang}}
                </span>
              </div>
            </div>
          </div>
        </div>
        @endif
        @endforeach
      </div>
    </div>
  </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Hapus Pesanan</h4>
      </div>
      <div class="modal-body">
        Apakah Anda Yakin ingin menghapus pesanan ini ?
        <input type="hidden" name="" value="" id="batal">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-pink" data-dismiss="modal" id="confirmbatal">Ya</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalBayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Bayar Pesanan</h4>
      </div>
      <div class="modal-body" style="height: 60vh">
        <div class="row m-0">
          @foreach($rekenings as $rekening)
          <div class="col p-3">
            <div class="col-rekening">
              <img src="{{asset('images/rekening')}}/{{$rekening->image}}">
              <div class="pt-2">{{$rekening->nomor}}</div>
            </div>
          </div>
          @endforeach
        </div>
        <div class="text-center font-26">
          <span id="totalBayar"></span>
        </div>
        <div class="text-center mb-5">
          <input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
          <img id="imgview" src="" class="" style="height:17rem" /><br>
          <label class="mt-2 btn-change text-center" for="imginput">
            <span class="btn border">Upload Bukti</span>
            <input type="hidden" name="" value="" id="bayar">
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-pink" data-dismiss="modal" id="confirmbayar">Bayar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Invoice Pesanan</h4>
      </div>
      <div class="modal-body" style="height: 60vh">
        <div class="text-center">
          <div class="font-44 text-bold" id="invoice">ACSBIWUE789985</div>
          <div class="font-18" id="nama">Nama : Ucok Bembem</div>
          <div class="font-18" id="namasalon">Salon : Ucok Bembem</div>
          <div class="font-18" id="idorder">Id Order : 3203224</div>
          <div class="font-18" id="jamdatang">Kedatangan : 3203224</div>
          <div class="font-18" id="noantri">Nomor Antri : 1</div>
        </div>
        <div class="mt-4 mb-2 text-bold font-16">Detail Pesanan :</div>
        <ul class="list-group"></ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalVerif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Bayar Pesanan</h4>
      </div>
      <div class="modal-body" style="height: 60vh">
        <input type="" name="" id="code" class="form-control font-30 p-5 text-center text-uppercase" placeholder="########">
      </div>
      <div class="modal-footer">
        <input type="hidden" id="id_verif" >
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
        <button type="button" class="btn btn-pink" data-dismiss="modal" id="verif_post">Verifikasi</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Rating Layanan</h4>
      </div>
      <div class="modal-body">
        <div class="font-24 text-center rate">
          <span id="star1" data="1" class="glyphicon rating glyphicon-star"></span>
          <span id="star2" data="2" class="glyphicon rating glyphicon-star"></span>
          <span id="star3" data="3" class="glyphicon rating glyphicon-star"></span>
          <span id="star4" data="4" class="glyphicon rating glyphicon-star"></span>
          <span id="star5" data="5" class="glyphicon rating glyphicon-star"></span>
        </div>
        <label>Ulasan</label>
        <textarea class="form-control" rows="4" id="ulasan"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-pink" data-dismiss="modal" id="confirrating">Kirim</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  //bayar dan batal
  var file;
  $('.bayar').click(function () {
    var id = $(this).attr('dataid');
    var bayar = $(this).attr('datatotal');
    $('#bayar').val(id);
    $('#totalBayar').text(convertToRupiah(bayar));
  });
  $("#imginput").on("change",function () {
    file=this.files[0];
  });
  $('.batal').click(function () {
    var id = $(this).attr('dataid');
    $('#batal').val(id);
  });
  $('.verif').click(function () {
    var id = $(this).attr('dataid');
    $('#id_verif').val(id);  
  });
  $('#confirmbayar').click(function () {
    if($('#imginput').val() == ''){
      alert('bukti kosong');
    }
    else{
      var id = $('#bayar').val();
      console.log(file);
      formdata=new FormData();
      formdata.append("_token","{{csrf_token()}}");
      formdata.append("image",file);
      $.ajax({
        url:"/user/upload/bukti/"+id,
        type:"POST",
        data:formdata,
        processData:false,
        contentType:false,
        success:function (data) {
          console.log(data);
          location.href="/user/order";
        }
      });
    }

    console.log('bayar id : ',id);
  });
  $('#confirmbatal').click(function () {
    var id = $('#batal').val();
    console.log('batal id : ',id);
    location.href="/user/cancel/order/"+id;
  });
  //selesai
  $('.invoice').click(function () {
    var nama = [];
    var jumlah = [];
    $('.list-group').empty();
    var id = $(this).attr('dataid');
    var invoice = $(this).attr('datainvoice');
    var idorder = $(this).attr('dataidorder');
    var jamdatang = $(this).attr('datajam');
    var antri = $(this).attr('dataantri');
    var salon = $(this).attr('datasalon');

    $('#invoice').text(invoice);
    $('#jamdatang').text('Kedatangan : '+jamdatang);
    $('#idorder').text('Id Order : '+idorder);
    $('#noantri').text('Nomor Antri : '+antri);
    $('#namasalon').text('Salon :  '+salon);
    $('#nama').text($('#username').val());

    $('.nama'+id).each(function () {
      nama.push($(this).val());
    });
    $('.qty'+id).each(function () {
      jumlah.push($(this).val());
    });
    for(i=0;i<nama.length;i++){
      $('.list-group').append(`
        <li class="list-group-item d-flex justify-content-between align-items-center">
        `+nama[i]+`
        <span class="badge badge-primary badge-pill">`+jumlah[i]+`</span>
        </li>
        `);
    }
  });
  $('#verif_post').click(function () {
    var id = $('#id_verif').val();
    var code = $('#code').val();
    formdata=new FormData();
    formdata.append("_token","{{csrf_token()}}");
    formdata.append("code",code);
    $.ajax({
      url:"/user/verif/order/"+id,
      type:"POST",
      data:formdata,
      processData:false,
      contentType:false,
      success:function (data) {
        console.log(data);
        location.href="/user/order";
      }
    })
  });


  /*  Rating*/
  var idorder = 0;
  $('.nilai').click(function () {
    idorder = $(this).attr('dataid');
  });
  var rate = 0;
  $('.rating').click(function () {
    rate = parseInt($(this).attr('data'));
    $('.rating').each(function () {
      $(this).removeClass('active');
    })
    for (i=1; i <= rate; i++){
      $('#star'+i).addClass('active');
    }
  });
  $('#confirrating').click(function () {
    formdata=new FormData();
    formdata.append("_token","{{csrf_token()}}");
    formdata.append("rate",rate);
    formdata.append("komentar",$('#ulasan').val());
    $.ajax({
      url:"/user/ulasan/order/"+idorder,
      type:"POST",
      data:formdata,
      processData:false,
      contentType:false,
      success:function (data) {
        console.log(data);
        location.href="/user/order";
      }
    })

  })
</script>
@endsection