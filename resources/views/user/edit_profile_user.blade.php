@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/user')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/user/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold pink">Profil Pengguna</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/user/order')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Pesanan</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-20">Edit Profile Pengguna</span>
      </div>
      <form method="post" action="{{url('user/update/profile')}}" enctype="multipart/form-data">
            {{csrf_field()}}
      <div class="row m-0 mt-4">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Nama Pengguna</div>
        <div class="col p-0 font-16">
          <input type="" name="name" class="form-control" style="width: 30rem" required value="{{$user->name}}">
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Telepon</div>
        <div class="col-md-9 p-0 font-16">
          <input type="" name="telepon" class="form-control" style="width: 30rem" required value="{{$user->telepon}}">
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Profile Pengguna</div>
        <div class="col-md-9 p-0">
          <div class="image-view-upload d-inline-block">
            <input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
            <img id="imgview" src="{{asset('images/user')}}/{{$user->image}}" class="profile-owner-upload ml-3"/><br>
            <label class="mt-2 btn-change text-center" for="imginput">
              <span class="btn border">Pilih Gambar</span>
            </label>
          </div>
        </div>
      </div>
      <div class="text-right">
        <button type="submit" class="btn btn-app">Simpan</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endsection