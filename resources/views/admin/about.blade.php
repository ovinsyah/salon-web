@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">About</div>
	<div class="text-right mb-3">
		<a href="{{url('/admin/edit/about')}}" class="btn btn-app">
			<i class="material-icons mr-2">edit</i>About
		</a>
	</div>
	<p>
		{!!$about->isi!!}
	</p>
</div>
@endsection