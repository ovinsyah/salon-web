@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-5">User</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama User</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td><a href="{{url('/admin/detail/user',$user->id)}}">{{$user->name}}</a></td>
				<td><a href="{{url('/admin/detail/user',$user->id)}}">{{$user->created_at}}</a></td>
				<td>
					<a href="{{url('/admin/detail/user',$user->id)}}">
						<i class="material-icons">visibility</i>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection