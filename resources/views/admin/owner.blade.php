@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-5">Owner Salon</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Salon</th>
				<th>Alamat</th>
				<th>Buka Salon</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($owners as $owner)
			<tr>
				<td><a href="{{('detail/owner/1')}}">{{$owner->name}}</a></td>
				<td><a href="{{('detail/owner/1')}}">{{$owner->alamat}}</a></td>
				<td><a href="{{('detail/owner/1')}}">{{$owner->jam_buka}} - {{$owner->jam_tutup}}</a></td>
				<td><a href="{{('detail/owner/1')}}">{{$owner->created_at}}</a></td>
				<td>
					<a href="{{('detail/owner/1')}}">
						<i class="material-icons">visibility</i>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection