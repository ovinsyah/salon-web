@extends('master.master_admin')
@section('content')
<div id="formApp">
	<div class="title-page-admin mb-4">Tambah Video</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Judul</span>
		</div>
		<div class="col p-0">
			<input type="" id="judul" class="form-control" style="width: 25rem;">
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Video</span>
		</div>
		<div class="col p-0">
			<input type="file" id="vid" name="vid" accept="video/mp4,video/*"  onchange="up_video()">
		</div>
	</div>
	<div class="text-right">
		<button class="btn btn-app disabled" id="confirm">Simpan</button>
	</div>
</div>

<script type="text/javascript">
	var video;
	function up_video() {
		video = event.target.files[0];
	}
	$(document).ready(function() {
		$('#adm-video').addClass('active');
		$('#confirm').on('click',function () {
			kirim();
			console.log(video,$('#judul').val())
		})
		function kirim() {
			formdata = new FormData();  
			if (formdata) {
				formdata.append("_token", "{{ csrf_token() }}");
				formdata.append("judul", $('#judul').val());
				formdata.append("video",video);
				
				$.ajax({
					url: "/admin/create/video",
					type: "POST",
					data: formdata,
					processData: false,
					contentType: false,
					success:function(data){
						location.href="/admin/video";
						console.log(data);
					}
				});
			}		
		}
	});
</script>
@endsection