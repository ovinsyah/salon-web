@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Pesanan Telah Bayar</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Pelanggan</th>
				<th>List Pesanan</th>
				<th>Salon</th>
				<th>Harga</th>
				<th>status</th>
				<th>Tanggal</th>
				<th width="40" class="text-left">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->user->name}}</a></td>
				<td>
					<a href="{{('/admin/detail/order')}}/{{$order->id}}">
						@foreach($order->list as $item)
						{{$item->layanan->nama}}          
						@endforeach          
					</a>
				</td>
				<td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->pemilik->name}}</a></td>
				<td>
					<a href="{{('/admin/detail/order')}}/{{$order->id}}">
						<?php
						$harga = 0;
						?>
						@foreach($order->list as $item)
						<?php
						$harga += ($item->layanan->harga-($item->layanan->diskon*$item->layanan->harga)/100)*$item->qty;
						?>
						@endforeach
						Rp {{number_format($harga,0,',','.')}}
					</a>
				</td>
				<td>{{($order->status==2)?'verif':'pending'}}</td>
				<td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->created_at}}</a></td>
				<td class="text-left">
					<a href="{{('/admin/detail/order')}}/{{$order->id}}">
						<i class="material-icons">visibility</i>
					</a>
					@if($order->status==1)
					<a dataname="{{$order->user->name}}" dataid="{{$order->id}}" href="" data-toggle="modal" data-target="#modalAcc" class="verif">
						<i class="material-icons">done</i>
					</a>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalAcc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Verifikasi Order</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin memverifikasi pesanan <span id="nameVerif"></span> ?
        <input type="hidden" name="" id="idVerif">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="confirmVerif">Ya</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();

	$('#adm-order').addClass('active');
	$('#adm-order').removeClass('collapsed');
	$('#pageSubmenu').attr( "aria-expanded", "true" );
	$('#pageSubmenu').addClass('in');
	$('#pageSubmenu').addClass('show');
	$('#adm-menu').attr( "aria-expanded", "true" );
	$('#adm-order-tb').addClass('active');

	$('.verif').click(function () {
    var id = $(this).attr('dataid');
    var name = $(this).attr('dataname');

    $('#nameVerif').text(name);
    $('#idVerif').val(id);
  });

  $('#confirmVerif').click(function () {
    var id = $('#idVerif').val();
    location.href="/admin/verif/order/"+id;
    console.log(id);
  })
</script>
@endsection