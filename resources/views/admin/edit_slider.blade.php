@extends('master.master_admin')
@section('content')
<div id="formAppEdit">
	<div class="title-page-admin mb-4">Edit SLider</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Judul</span>
		</div>
		<div class="col p-0">
			<input type="" id="judul" class="form-control" style="width: 25rem;" value="{{$slider->judul}}">
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Sampul</span>
		</div>
		<div class="col p-0">
			<div class="image-view-upload d-inline-block">
				<input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
				<img id="imgview" src="{{asset('images/slider')}}/{{$slider->sampul}}" class="sampul-upload"/>
				<div class="btn-upload-sampul text-center">
				<label class="mt-2 btn-change text-center" for="imginput">
					<span class="btn">Pilih Gambar</span>
				</label>
			</div>
			</div>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Deskripsi</span>
		</div>
		<div class="col p-0">
			<div id="deskripsi" style="height: 17rem"></div>
		</div>
	</div>
	<div class="text-right">
		<button class="btn btn-app mt-3" id="confirm">Simpan</button>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#adm-slider').addClass('active');
		$('#deskripsi').summernote('code', `{!!$slider->deskripsi!!}`);

		var sampul;
		$("#imginput").on("change", function() {
			sampul = this.files[0];
		});

		$('#confirm').on('click',function (argument) {
			formdata = new FormData();  
			if (formdata) {
				formdata.append("_token", "{{ csrf_token() }}");
				formdata.append("judul", $('#judul').val());
				formdata.append("sampul",sampul);
				formdata.append("isi", $('#deskripsi').summernote('code'));	
			
				$.ajax({
					url: "/admin/update/slider/"+"{{$slider->id}}",
					type: "POST",
					data: formdata,
					processData: false,
					contentType: false,
					success:function(data){
						location.href="/admin/slider";
						console.log(data);
					}
				});
			}
			console.log($('#deskripsi').summernote('code'));
		});
	});
</script>
@endsection