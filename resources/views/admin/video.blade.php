@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Video</div>
	<div class="text-right mb-3">
		<a href="{{url('/admin/add/video')}}" class="btn btn-app">
			<i class="material-icons mr-2">control_point</i>Video
		</a>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Video</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($videos as $video)
			<tr>
				<td><a href="{{url('admin/detail/video',$video->id)}}">{{$video->judul}}</a></td>
				<td><a href="{{url('admin/detail/video',$video->id)}}">{{$video->created_at}}</a></td>
				<td>
					<a href="{{url('admin/detail/video',$video->id)}}">
						<i class="material-icons">visibility</i>
					</a>
					<a data-toggle="modal" data-target="#modalRemove{{$video->id}}">
						<i class="material-icons">delete</i>
					</a>
				</td>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$video->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus Video </h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus {{$video->judul}}</span> ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <a href="{{url('admin/delete/video',$video->id)}}" class="btn btn-primary" id="confirm">Ya</a>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection