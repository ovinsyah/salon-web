@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-5">Dashboard</div>
	<div class="row m-0">
		<div class="col p-2">
			<a href="{{url('admin/owner')}}">
			<div class="col-dashboard-admin">
			<i class="material-icons font-80">supervisor_account</i>
			<div class="font-26">{{count(App\Pemilik::all())}}</div><div class="font-18">Owner Salon</div>
			</div>
			</a>
		</div>
		<div class="col p-2">
			<a href="{{url('admin/user')}}">
			<div class="col-dashboard-admin">
			<i class="material-icons font-80">person</i>
			<div class="font-26">{{count(App\User::all())}}</div><div class="font-18">User</div>
			</div>
			</a>
		</div>
		<div class="col p-2">
			<a href="{{url('admin/slider')}}">
			<div class="col-dashboard-admin">
			<i class="material-icons font-80">style</i>
			<div class="font-26">{{count(App\Article::all())}}</div><div class="font-18">Artikel</div>
			</div>
			</a>
		</div>
		<div class="col p-2">
			<a href="{{url('admin/video')}}">
			<div class="col-dashboard-admin">
			<i class="material-icons font-80">movie</i>
			<div class="font-26">{{count(App\Video::all())}}</div><div class="font-18">Video</div>
			</div>
			</a>
		</div>
		<div class="col p-2">
			<a href="{{url('admin/order/belum-bayar')}}">
			<div class="col-dashboard-admin">
			<i class="material-icons font-80">payment</i>
			<div class="font-26">{{count(App\Terjual::all())}}</div><div class="font-18">Pesanan</div>
			</div>
			</a>
		</div>
	</div>
</div>
@endsection