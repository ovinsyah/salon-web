@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Pesanan Batal</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Pelanggan</th>
        <th>List Pesanan</th>
        <th>Salon</th>
				<th>Harga</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->user->name}}</a></td>
				<td>
					<a href="{{('/admin/detail/order')}}/{{$order->id}}">
						@foreach($order->list as $item)
						{{$item->layanan->nama}}          
						@endforeach          
					</a>
				</td>
				<td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->pemilik->name}}</a></td>
				<td>
					<a href="{{('/admin/detail/order')}}/{{$order->id}}">
						<?php
						$harga = 0;
						?>
						@foreach($order->list as $item)
						<?php
						$harga += ($item->layanan->harga-($item->layanan->diskon*$item->layanan->harga)/100)*$item->qty;
						?>
						@endforeach
						Rp {{number_format($harga,0,',','.')}}
					</a>
				</td>
				<td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->created_at}}</a></td>
				<td>
					<a href="{{('/admin/detail/order')}}/{{$order->id}}">
		            <i class="material-icons">visibility</i>
		            </a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();

  $('#adm-order').addClass('active');
  $('#adm-order').removeClass('collapsed');
  $('#pageSubmenu').attr( "aria-expanded", "true" );
  $('#pageSubmenu').addClass('in');
  $('#pageSubmenu').addClass('show');
  $('#adm-menu').attr( "aria-expanded", "true" );
  $('#adm-order-b').addClass('active');
</script>
@endsection