@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Artikel</div>
	<div class="text-right mb-3">
		<a href="{{url('/admin/add/article')}}" class="btn btn-app">
			<i class="material-icons mr-2">control_point</i>Artikel
		</a>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Artikel</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($articles as $article)
			<tr>
				<td><a href="{{url('admin/detail/article',$article->id)}}">{{$article->judul}}</a></td>
				<td><a href="{{url('/detail/article',$article->id)}}">{{$article->created_at}}</a></td>
				<td>
					<a href="{{url('admin/detail/article',$article->id)}}">
						<i class="material-icons">visibility</i>
					</a>
					<a href="{{url('admin/edit/article',$article->id)}}">
						<i class="material-icons">edit</i>
					</a>
					<a href="" data-toggle="modal" data-target="#modalRemove{{$article->id}}">
						<i class="material-icons">delete</i>
					</a>
				</td>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus Artikel</h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus <span id="nameThis"></span> ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <a href="{{url('admin/delete/article',$article->id)}}" class="btn btn-primary" id="confirm">Ya</a>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection