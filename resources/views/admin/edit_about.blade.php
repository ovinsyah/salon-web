@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Edit About</div>
	<div id="deskripsi"></div>
	<div class="text-right">
		<button class="btn btn-app" id="confirm">Simpan</button>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#adm-about').addClass('active');
		$('#deskripsi').summernote('code', `{!!$about->isi!!}`);

		$('#confirm').on('click',function (argument) {
			formdata = new FormData();  
			if (formdata) {
				formdata.append("_token", "{{ csrf_token() }}");
				formdata.append("isi", $('#deskripsi').summernote('code'));
			
				$.ajax({
					url: "/admin/update/about",
					type: "POST",
					data: formdata,
					processData: false,
					contentType: false,
					success:function(data){
						location.href="/admin/about";
						console.log(data);
					}
				});
			}
			console.log($('#deskripsi').summernote('code'));
		});
	});
</script>
@endsection