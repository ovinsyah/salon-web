@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3" >Rekening</div>
	<div class="text-right mb-3">
		<a href="" class="btn btn-app" data-toggle="modal" data-target="#modalAdd">
			<i class="material-icons mr-2">control_point</i>Rekening
		</a>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Rekening</th>
				<th>Nomor Rekening</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($rekenings as $rekening)
			<tr>
				<td>{{$rekening->nama}}</td>
				<td  >{{$rekening->nomor}}</td>
				<td>{{$rekening->created_at}}</td>
				<td>
					<a dataid="{{$rekening->id}}" dataname="{{$rekening->nama}}" datano="{{$rekening->nomor}}" dataimg="{{$rekening->image}}" href="" data-toggle="modal"  data-target="#modalEdit" class="edit">
						<i class="material-icons" >edit</i>
					</a>
					<a href="" data-toggle="modal" data-target="#modalRemove{{$rekening->id}}">
						<i class="material-icons">delete</i>
					</a>
				</td>
			</tr>
    <!-- Modal -->
    <div class="modal fade" id="modalRemove{{$rekening->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Hapus Rekening</h4>
          </div>
          <div class="modal-body">
            Apakah anda yakin menghapus <span id="nameThis"></span> ?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            <a  href="{{url('admin/delete/rekening',$rekening->id)}}" class="btn btn-primary" >Ya</a>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </tbody>
</table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Rekening</h4>
      </div>
      <div class="modal-body">
      	<label>Nama Rekening</label>
      	<input type="" id="add_nama" class="form-control">
        <label class="mt-3">Nomor Rekening</label>
        <input type="" id="add_no" class="form-control">
        <label class="mt-3">Gambar Rekening</label>
        <div class="image-view-upload text-center">
          <input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
          <img id="imgview" src="{{asset('img/noimage.jpg')}}" class="image-upload"/><br>
          <label class="mt-2 btn-change text-center" for="imginput">
            <span class="btn btn-upload">Pilih Gambar</span>
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="add">Ya</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Edit Rekening</h4>
    </div>
    <div class="modal-body">
      <label>Nama Rekening</label>
      <input type="" id="edit_nama" class="form-control" value="">
      <label class="mt-3">Nomor Rekening</label>
      <input type="" id="edit_no" class="form-control" value="">
      <label class="mt-3">Gambar Rekening</label>
      <div class="image-view-upload text-center">
        <input name="image" type="file" accept="image/*" onchange="uploadimage2()" id="imginput2" class="hidden" />
        <img id="imgview2" src="" class="image-upload"/><br>
        <label class="mt-2 btn-change text-center" for="imginput2">
          <span class="btn btn-upload">Pilih Gambar</span>
        </label>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
      <button type="button" class="btn btn-primary" id="simpanedit">Ya</button>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready( function () {
   $('#table_id').DataTable();
   var update_id;

   function update(id) {
    update_id=id;
    console.log(update_id)
  }
  var image;
  $("#imginput").on("change", function() {
    image = this.files[0];
  });

  $('#add').on('click',function (argument) {
    formdata = new FormData();  
    if (formdata) {
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("add_nama", $('#add_nama').val());
      formdata.append("add_no", $('#add_no').val());
      formdata.append("image",image);
      
      $.ajax({
        url: "/admin/create/rekening",
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        success:function(data){
          location.href="/admin/rekening";
          console.log(data);
        }
      });
    }
  });

  var image2;
  $("#imginput2").on("change", function() {
    image2 = this.files[0];
  });
    $('.edit').click(function () {
      var id =  $(this).attr('dataid');
      var nama =  $(this).attr('dataname');
      var nomor =  $(this).attr('datano');
      var img =  $(this).attr('dataimg');

      $('#edit_nama').val(nama);
      $('#edit_no').val(nomor);
      $('#imgview2').attr('src','/images/rekening/'+img);
    });
  $('#simpanedit').on('click',function (argument) {
    formdata = new FormData();  
    if (formdata) {
      formdata.append("_token", "{{ csrf_token() }}");
      formdata.append("edit_nama", $('#edit_nama').val());
      formdata.append("edit_no", $('#edit_no').val());
      formdata.append("image",image2);
      
      $.ajax({
        url: "/admin/update/rekening/"+update_id,
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        success:function(data){
          location.href="/admin/rekening";
          console.log(data);
        }
      });
    }
  });
});
</script>
@endsection