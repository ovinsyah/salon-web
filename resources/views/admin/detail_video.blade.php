@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-4">Detail Video</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Judul</span>
		</div>
		<div class="col p-0">
			<span class="font-16 pt-2">{{$video->judul}}</span>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Video</span>
		</div>
		<div class="col p-0">
			<video controls>
				<source src="{{asset('video')}}/{{$video->video}}" type="video/mp4">
			</video>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#adm-video').addClass('active');
	});
</script>
@endsection