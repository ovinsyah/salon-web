@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-4">Detail SLider</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Judul</span>
		</div>
		<div class="col p-0">
			<span class="font-16 pt-2">{{$slider->judul}}</span>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Sampul</span>
		</div>
		<div class="col p-0">
			<img id="imgview" src="{{asset('images/slider')}}/{{$slider->sampul}}" class="sampul-upload"/>
		</div>
	</div>
	<div class="row m-0 mb-3">
		<div class="col p-0" style="max-width: 15rem">
			<span class="text-bold font-16 pt-2">Deskripsi</span>
		</div>
		<div class="col p-0">
			{!!$slider->deskripsi!!}
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#adm-slider').addClass('active');
	});
</script>
@endsection