@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Detail Pesanan</div>
	<div class="row m-0 mt-5">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Nama Pelanggan</div>
		<div class="col p-0 font-16">{{$order->user->name}}</div>
	</div>
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Nomor Telepon</div>
		<div class="col p-0 font-16">{{$order->telepon}}</div>
	</div>
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Salon Tujuan</div>
		<div class="col p-0 font-16">{{$order->pemilik->name}}</div>
	</div>
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Nommor Invoice</div>
		<div class="col p-0 font-16">{{$order->invoice}}</div>
	</div>
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Kedatangan</div>
		<div class="col p-0 font-16">{{$order->jam_datang}}</div>
	</div>
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Tanggal Diibuat Pesanan</div>
		<div class="col p-0 font-16">{{$order->created_at}}</div>
	</div>
	<?php 
		$total = 0;
	 ?>
	 @foreach($order->list as $item)
	<?php
		$total += ($item->layanan->harga-($item->layanan->diskon*$item->layanan->harga)/100)*$item->qty;
	 ?>
	 @endforeach
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Total Harga</div>
		<div class="col p-0 font-16">Rp {{number_format($total,0,',','.')}}</div>
	</div>
		@if($order->bukti != '' )
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Bukti</div>
		<div class="col p-0 font-16">
			 <img src="{{asset('images/bukti')}}/{{$order->bukti}}">
		</div>
	</div>
	@endif
	<div class="row m-0 mt-3">
		<div class="col p-0 font-16 text-bold" style="max-width: 22rem">Detail Pesanan</div>
		<div class="col p-0 font-16">
			<table class="table">
			    <thead>
			      <tr>
			        <th>Nama Layanan</th>
			        <th>Jumlah</th>
			        <th>Harga</th>
			      </tr>
			    </thead>
			    <tbody>
			    	@foreach($order->list as $item)
			    	<?php
			    		$total += $item->layanan->harga;
			    	 ?>
			      <tr>
			        <td>{{$item->layanan->nama}}</td>
			        <td>{{$item->qty}}</td>
			        <td>
			        	@if($item->layanan->diskon != 0)
			        	<span class="price-old mr-3">Rp {{number_format($item->layanan->harga,0,',','.')}}</span>
			        	@endif
			        	<span>Rp {{number_format($item->layanan->harga-($item->layanan->diskon*$item->layanan->harga)/100,0,',','.')}}</span>
			        </td>
			      </tr>
			      @endforeach
			    </tbody>
			  </table>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();

  $('#adm-order').addClass('active');
  $('#adm-order').removeClass('collapsed');
  $('#pageSubmenu').attr( "aria-expanded", "true" );
  $('#pageSubmenu').addClass('in');
  $('#pageSubmenu').addClass('show');
  $('#adm-menu').attr( "aria-expanded", "true" );
</script>
@endsection