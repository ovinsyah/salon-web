@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Slider</div>
	<div class="text-right mb-3">
		<a href="{{url('/admin/add/slider')}}" class="btn btn-app">
			<i class="material-icons mr-2">control_point</i>Slider
		</a>
	</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama SLider</th>
				<th>Tanggal</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($sliders as $slider)
			<tr>
				<td><a href="{{url('/admin/detail/slider',$slider->id)}}">{{$slider->judul}}</a></td>
				<td><a href="{{url('/admin/detail/slider',$slider->id)}}">{{$slider->created_at}}</a></td>
				<td>
					<a href="{{url('/admin/detail/slider',$slider->id)}}">
						<i class="material-icons">visibility</i>
					</a>
					<a href="{{url('/admin/edit/slider',$slider->id)}}">
						<i class="material-icons">edit</i>
					</a>
					<a href="" data-toggle="modal" data-target="#modalRemove{{$slider->id}}">
						<i class="material-icons">delete</i>
					</a>
				</td>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="modalRemove{{$slider->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Hapus Slider</h4>
			      </div>
			      <div class="modal-body">
			        Apakah anda yakin menghapus {{$slider->judul}} ?
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
			        <button type="button" class="btn btn-primary" id="confirm">Ya</button>
			      </div>
			    </div>
			  </div>
			</div>
			@endforeach
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();
</script>
@endsection