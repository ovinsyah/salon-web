@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Detail Salon</div>
	<div class="row m-0">
		<div class="col p-0" style="max-width: 12rem"><img src="{{asset('images/owner')}}/{{$owners->image}}" class="img-profile"></div>
		<div class="col p-0 pt-4">
			<div class="text-bold font-18">{{$owners->name}}</div>
			<div class="font-14"><b>Email :</b> {{$owners->email}}</div>
			<div class="font-14"><b>Telepon :</b> {{$owners->telepon}}</div>
		</div>
	</div>
	<div class="row m-0 mt-2">
		<div class="col-md-7">
			<div class="thumb-salon-open">
	        <span class="open-salon">Buka</span>
	        <span>{{$owners->jam_buka}} - {{$owners->jam_tutup}}</span>

	        <div class="font-16 mt-1">
     		<span>Lokasi</span>
		    </div>
		    <div class="text-thin">{{$owners->alamat}}</div>
		    <div id="map" class="mb-5" style="height: 30rem"></div>

      </div>
		</div>
		<div class="col-md-5" style="background: #f8f8f8">
			<div class="text-bold font-16 mb-2">Daftar Paket :</div>
			@foreach($owners->service as $service)
      <div class="row m-0 mb-3">
      <div class="col p-0 text-center" style="max-width: 90px">
        <img src="{{asset('images/service')}}/{{$service->sampul}}" class="img-menu-service">
        <input type="hidden" value="{{asset('images/service')}}/{{$service->sampul}}" name="" id="gbr{{$service->id}}">
      </div>
      <div class="col">
        <a href="{{url('detail/service/1')}}">
          <div class="title-menu-service">
            {{$service->nama}}
          </div>
          <span class="">Rp. {{$service->harga}}</span>
          <div class="desc-menu-service">
            {{$service->deskripsi}}
            <input type="hidden" name="" value="{{$service->deskripsi}}" id="desc{{$service->id}}">
          </div>
        </a>
      </div>
    </div>
      @endforeach
		</div>
	</div>
</div>
<input type="hidden" name="" value="{{$owners->latlng}}" id="latlng">
<script type="text/javascript">
	$('#adm-owner').addClass('active');
	function initMap() {
    var tmplatlng = $('#latlng').val();
    var latlng = tmplatlng.split(",");
    var x = parseFloat(latlng[0]);
    var y = parseFloat(latlng[1]);
    var myLatLng = {lat: x, lng: y};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: myLatLng
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Hello World!'
    });
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection