@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Detail User</div>
		<div class="row m-0">
		<div class="col p-0" style="max-width: 12rem"><img src="{{asset('images/user')}}/{{$user->image}}" class="img-profile"></div>
		<div class="col p-0 pt-4">
			<div class="text-bold font-18">{{$user->name}}</div>
			<div class="font-14"><b>Email :</b> {{$user->email}}</div>
			<div class="font-14"><b>Telepon :</b> {{$user->telepon}}</div>
		</div>
	</div>
</div>
<div class="pb-2 mb-4" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-16">Pesanan Pengguna</span>
      </div>
      <ul class="nav nav-tabs border-0 row m-0">
        <li class="active col p-0 text-center">
          <a class="tabs-login" data-toggle="tab" href="#belumbayar">Belum Bayar</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login" data-toggle="tab" href="#telahbayar">Telah Bayar</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login" data-toggle="tab" href="#telahditerima">Telah Diterima</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login" data-toggle="tab" href="#selesai">Selesai</a>
        </li>
        <li class=" col p-0 text-center">
          <a class="tabs-login" data-toggle="tab" href="#batal">Batal</a>
        </li>
      </ul>

      <div class="tab-content">
        <div id="belumbayar" class="tab-pane fade in active">
          @foreach($user->beli as $order)
          @if($order->status==0)
          <div class="row m-0 list-order">
            <div class="col p-0 user-order-img">
             <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
            </div>
            <div class="col p-0">
            	<?php
            		$total = 0;
            	?>
              @foreach($order->list as $item)
               <?php
            		$total += $item->qty*$item->layanan->harga;
            	?>
              <div class="row m-0 mb-2">
                <div class="col p-0">
                  {{$item->layanan->nama}}
                </div>
                <div class="col p-0 text-center">
                  x{{$item->qty}}
                </div>
                <div class="col p-0 text-right">
                  Rp {{number_format($item->layanan->harga,0,',','.')}}
                </div>
              </div>
              @endforeach
              <div class="col-12 p-0 list-order-total">
                Total : Rp {{number_format($total,0,',','.')}}
              </div>
            </div>
          </div>
          @endif
          @endforeach
        </div>
        <div id="telahbayar" class="tab-pane fade">
        @foreach($user->beli as $order)
          @if($order->status==1 )
          <div class="row m-0 list-order">
            <div class="col p-0 user-order-img">
              <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
            </div>
            <div class="col p-0">
            	<?php
            		$total = 0;
            	?>
              @foreach($order->list as $item)
              <?php
            		$total += $item->qty*$item->layanan->harga;
            	?>
              <div class="row m-0 mb-2">
                <div class="col p-0">
                  {{$item->layanan->nama}}
                </div>
                <div class="col p-0 text-center">
                  x{{$item->qty}}
                </div>
                <div class="col p-0 text-right">
                  Rp {{number_format($item->layanan->harga,0,',','.')}}
                </div>
              </div>
              @endforeach
              <div class="col-12 p-0 list-order-total">
                Total : Rp {{number_format($total,0,',','.')}}
              </div>
            </div>
          </div>
          @endif
          @endforeach
        </div>
        <div id="telahditerima" class="tab-pane fade">
        @foreach($user->beli as $order)
          @if($order->status==2)
          <div class="row m-0 list-order">
            <div class="col p-0 user-order-img">
              <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
            </div>
            <div class="col p-0">
              @foreach($order->list as $item)
              <div class="row m-0 mb-2">
                <div class="col p-0">
                  {{$item->layanan->nama}}
                  <input type="hidden" name="" value="{{$item->layanan->nama}}" class="nama{{$order->id}}">
                </div>
                <div class="col p-0 text-center">
                  x{{$item->qty}}
                  <input type="hidden" name="" value="{{$item->qty}}" class="qty{{$order->id}}">
                </div>
                <div class="col p-0 text-right">
                  Rp {{number_format($item->layanan->harga,0,',','.')}}
                </div>
              </div>
              @endforeach
              <div class="col-12 p-0 list-order-total">
                Total : Rp {{number_format($total,0,',','.')}}
              </div>
            </div>
          </div>
          @endif
          @endforeach
        </div>
        <div id="selesai" class="tab-pane fade">
        @foreach($user->beli as $order)
          @if($order->status==3)
          <div class="row m-0 list-order">
            <div class="col p-0 user-order-img">
              <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
            </div>
            <div class="col p-0">
              @foreach($order->list as $item)
              <div class="row m-0 mb-2">
                <div class="col p-0">
                  {{$item->layanan->nama}}
                </div>
                <div class="col p-0 text-center">
                  x{{$item->qty}}
                </div>
                <div class="col p-0 text-right">
                  Rp {{number_format($item->layanan->harga,0,',','.')}}
                </div>
              </div>
              @endforeach
              <div class="col-12 p-0 list-order-total">
                Total : Rp {{number_format($total,0,',','.')}}
              </div>
            </div>
          </div>
          @endif
          @endforeach
        </div>
        <div id="batal" class="tab-pane fade">
        @foreach($user->beli as $order)
          @if($order->status==4)
          <div class="row m-0 list-order">
            <div class="col p-0 user-order-img">
              <img src="{{asset('images/service')}}/{{$order->list[0]->layanan->sampul}}">
            </div>
            <div class="col p-0">
              @foreach($order->list as $item)
              <div class="row m-0 mb-2">
                <div class="col p-0">
                  {{$item->layanan->nama}}
                </div>
                <div class="col p-0 text-center">
                  x{{$item->qty}}
                </div>
                <div class="col p-0 text-right">
                  Rp {{number_format($item->layanan->harga,0,',','.')}}
                </div>
              </div>
              @endforeach
              <div class="col-12 p-0 list-order-total">
                Total : Rp {{number_format($total,0,',','.')}}
              </div>
            </div>
          </div>
          @endif
          @endforeach
        </div>
      </div>
<script type="text/javascript">
	$('#adm-user').addClass('active');
</script>
@endsection