@extends('master.master_admin')
@section('content')
<div>
	<div class="title-page-admin mb-3">Pesanan Belum Bayar</div>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Nama Pelanggan</th>
        <th>List Pesanan</th>
        <th>Salon</th>
        <th>Harga</th>
        <th>Tanggal</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
     @foreach($orders as $order)
     <tr>
      <td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->user->name}}</a></td>
      <td>
        <a href="{{('/admin/detail/order')}}/{{$order->id}}">
          @foreach($order->list as $item)
          {{$item->layanan->nama}}          
          @endforeach          
        </a>
      </td>
      <td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->pemilik->name}}</a></td>
      <td>
        <a href="{{('/admin/detail/order')}}/{{$order->id}}">
          <?php
          $harga = 0;
           ?>
          @foreach($order->list as $item)
              <?php
                $harga += ($item->layanan->harga-($item->layanan->diskon*$item->layanan->harga)/100)*$item->qty;
              ?>
          @endforeach
          Rp {{number_format($harga,0,',','.')}}
        </a>
      </td>
      <td><a href="{{('/admin/detail/order')}}/{{$order->id}}">{{$order->created_at}}</a></td>
      <td>
        <a href="{{('/admin/detail/order')}}/{{$order->id}}">
          <i class="material-icons">visibility</i>
        </a>
        <a dataname="{{$order->user->name}}" dataid="{{$order->id}}" href="" data-toggle="modal" data-target="#modalRemove" class="delete">
          <i class="material-icons">delete</i>
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus Order</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin menghapus pesanan <span id="nameDelete"></span> ?
        <input type="hidden" name="" id="idDelete">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="confirmDelete">Ya</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('#table_id').DataTable();

  $('#adm-order').addClass('active');
  $('#adm-order').removeClass('collapsed');
  $('#pageSubmenu').attr( "aria-expanded", "true" );
  $('#pageSubmenu').addClass('in');
  $('#pageSubmenu').addClass('show');
  $('#adm-menu').attr( "aria-expanded", "true" );
  $('#adm-order-bb').addClass('active');

  $('.delete').click(function () {
    var id = $(this).attr('dataid');
    var name = $(this).attr('dataname');

    $('#nameDelete').text(name);
    $('#idDelete').val(id);
  });

  $('#confirmDelete').click(function () {
    var id = $('#idDelete').val();
    console.log(id);
  })
</script>
@endsection