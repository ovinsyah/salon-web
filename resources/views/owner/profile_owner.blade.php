@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold pink">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
       @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
        </div>
      @elseif ($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
        </div>
      @endif
      <div class="row m-0 pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <div class="col p-0">
          <span class="text-bold font-20">Profile Salon</span>
        </div>
        <div class="col p-0 text-right">
          <a href="{{url('/owner/edit/profile')}}" class="btn btn-app pt-1 pb-1 font-16"><i class="material-icons">edit</i> Edit</a>
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Nama Salon</div>
        <div class="col p-0 font-16">{{Auth::user()->name}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Email</div>
        <div class="col p-0 font-16">{{Auth::user()->email}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Telepon</div>
        <div class="col p-0 font-16">{{Auth::user()->telepon}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Profile Salon</div>
        <div class="col p-0">
          <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner-upload">
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Password</div>
        <div class="col p-0 font-16 text-bold">
          <span>*********</span><a class="font-14 ml-3" href="{{url('/owner/change-password')}}" style="position:  relative;top: -4px;color: #0e6fb7;">Ganti</a>
        </div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Salon Buka</div>
        <div class="col p-0 font-16">{{Auth::user()->jam_buka}} - {{Auth::user()->jam_tutup}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Alamat Salon</div>
        <div class="col p-0 font-16">
          <div class="mb-3">
            {{Auth::user()->alamat}}
          </div>
          <div id="map" style="height: 30rem"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="" value="{{Auth::user()->latlng}}" id="latlng">
<script>
  function initMap() {
/*    var latlng="{{Auth::user()->latlng}}";
    var myLatLng = {lat: parseFloat(latlng.split(',')[0]), lng:  parseFloat(latlng.split('|')[1])};*/
    var tmplatlng = $('#latlng').val();
    var latlng = tmplatlng.split(",");
    var x = parseFloat(latlng[0]);
    var y = parseFloat(latlng[1]);
    var myLatLng = {lat: x, lng: y};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: myLatLng
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Hello World!'
    });
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMku8Cv-ld6KagEfwMCmyi4G997TAAZPo&callback=initMap">
</script>
@endsection