@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold pink">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-20">Edit Profile Salon</span>
      </div>
      <form method="post" action="{{url('owner/update/profile')}}" enctype="multipart/form-data">
        {{csrf_field()}}      
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Nama Salon</div>
          <div class="col p-0 font-16">
            <input type="" name="name" class="form-control" style="width: 30rem" required value="{{$user->name}}">
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Telepon</div>
          <div class="col p-0 font-16">
            <input type="" name="telepon" class="form-control" style="width: 30rem" required value="{{$user->telepon}}">
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 text-bold" style="max-width:  16rem;">Profile Salon</div>
          <div class="col p-0">
            <div class="image-view-upload d-inline-block">
              <input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
              <img id="imgview" src="{{asset('images/owner')}}/{{$user->image}}" class="profile-owner-upload ml-3"/><br>
              <label class="mt-2 btn-change text-center" for="imginput">
                <span class="btn border">Pilih Gambar</span>
              </label>
            </div>
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 text-bold" style="max-width:  16rem;">Salon Buka</div>
          <div class="col p-0 row m-0">

            <div class="col p-0 input-group clockpicker" style="max-width: 10rem">
              <input type="text" class="form-control" value="{{$user->jam_buka}}" name="jam_buka">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
              </span>
            </div>
            <div class="col p-0 pt-3 text-center font-16" style="max-width: 5rem">s/d</div>
            <div class="col p-0 input-group clockpicker2" style="max-width: 10rem">
              <input type="text" class="form-control" value="{{$user->jam_tutup}}" name="jam_tutup">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-time"></span>
              </span>
            </div>
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 text-bold" style="max-width:  16rem;">Alamat Salon</div>
          <div class="col p-0 font-16">
            <span id="address">
              {{$user->alamat}}
            </span>
            <div class="row m-0">
              <div class="col-6 p-0">
                <input type="text" class="form-control" id="location-text-box" name="alamat"  value="" />
                <input type="hidden" class="form-control" id="latlng" name="latlng" value="{{$user->latlng}}" />
              </div>
              <div class="col-6 text-right p-0">
                <a class="btn btn-default" id="set-address">Ambil Lokasi</a>
              </div>
            </div>
            <p class="mt-3" id="map-canvas" style="width:100%; height:30rem;"></p>
          </div>
        </div>
        <div class="text-right">
          <button class="btn btn-app">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('.clockpicker').clockpicker({
    placement: 'top',
    align: 'left',
    donetext: 'Done'
  });
  $('.clockpicker2').clockpicker({
    placement: 'top',
    align: 'left',
    donetext: 'Done'
  });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOUtDxhLWJAwwg4qr9R70H1V37i83vWks&libraries=geometry,places"></script>
<script type="text/javascript">
  var map,marker,loc,tmploc,jalan,kelurahan,kecamatan,kota,provinsi,negara;
  if($('#latlng').val() == ''){
    tmploc=new google.maps.LatLng(3.5891272,98.6939443);
  }
  else{
    var latlng = $('#latlng').val().split(',');
    tmploc=new google.maps.LatLng(latlng[0],latlng[1]);
  }
  function initialize() {
    $("#set-address").click(function() {
      var geocoder = new google.maps.Geocoder;
      var infowindow = new google.maps.InfoWindow;
      geocodeLatLng(geocoder, infowindow);
    });

    var mapOptions = {
      zoom: 15
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  // Get GEOLOCATION
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
        position.coords.longitude);
      loc=pos;

      map.setCenter(pos);
      marker = new google.maps.Marker({
        position: pos,
        map: map,
        draggable: true
      });
      marker.addListener('dragend', function(event) {
        loc= event.latLng;
      });
      
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }



  function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
      var content = 'Error: The Geolocation service failed.';
    } else {
      var content = 'Error: Your browser doesn\'t support geolocation.';
    }
    loc= tmploc;
    map.setCenter(loc);
    var options = {
      map: map,
      position:loc ,
      content: content
    };

    map.setCenter(options.position);
    marker = new google.maps.Marker({
      position: options.position,
      map: map,
      draggable: true
    });
    marker.addListener('dragend', function(event) {
      console.log(event.latLng.lat(),event.latLng.lng())
      loc= event.latLng;
    });
  }

  // get places auto-complete when user type in location-text-box
  var input = (document.getElementById('location-text-box'));


  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29),
    draggable: true
  });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17); // Why 17? Because it looks good.
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    loc=place.geometry.location;

    var address = '';
    if (place.address_components) {
      address = [
      (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

  });
}

function geocodeLatLng(geocoder, infowindow) {

  geocoder.geocode({'location': loc}, function(results, status) {
    $('#latlng').val(loc.lat()+','+loc.lng());
    console.log($('#latlng').val(),loc);
    var alamat = results[0].formatted_address.split(',');
    jalan =alamat[0];
    kelurahan =alamat[1];
    kecamatan =alamat[2];
    kota =alamat[3];
    provinsi =alamat[4];
    negara =alamat[5];
    $('#address').text(jalan+','+kelurahan+','+kecamatan+','+kota+','+provinsi+','+negara);
    $('#location-text-box').val(jalan+','+kelurahan+','+kecamatan+','+kota+','+provinsi+','+negara);
    console.log(jalan,',',kelurahan,',',kecamatan,',',kota,',',provinsi,',',negara);
    if (status === 'OK') {
      if (results[0]) {
        map.setZoom(15);
      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>
@endsection