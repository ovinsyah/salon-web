@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold pink">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2 mb-4" style="border-bottom: 1px solid#d0d0d0;">
        <div class="row m-0">
          <div class="col p-0">
          <span class="text-bold font-20">Item yang telah selesai</span>
          </div>
          <div class="col p-0 text-right">
            <a class="btn btn-app" href="{{url('/owner/history-selesai')}}">Terjual</a>
          </div>
        </div>
      </div>
      <table id="table_id" class="display">
    <thead>
      <tr>
        <th>ID Order</th>
        <th>Invoice</th>
        <th>Pembeli</th>
        <th>Jumlah Item</th>
        <th>Kode Claim</th>
        <th>Nomor Antrian</th>
        <th>Jam Datang</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      @foreach($orders as $order)
      <tr>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->id}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->invoice}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->user->name}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->list->sum('qty')}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->unik_code}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->no_antrian}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}">{{$order->jam_datang}}</a></td>
        <td><a href="{{url('/owner/detail/order',$order->id)}}"> <i class="material-icons">visibility</i></a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#table_id').DataTable();
</script>
@endsection