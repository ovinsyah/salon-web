@extends('master.master_main')
@section('content')
<div class="col-home mt-5" id="formApp">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold pink">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-20">Ganti Password</span>
      </div>
      <form method="post" action="{{url('owner/update/password')}}" enctype="multipart/form-data">
        {{csrf_field()}} 
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Password Lama</div>
          <div class="col p-0 font-16">
            <input type="password" name="old" class="form-control" style="width: 30rem" required>
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Password Baru</div>
          <div class="col p-0 font-16">
            <input type="password" name="new" id="pass" class="form-control" style="width: 30rem" required>
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Konfirmasi Password</div>
          <div class="col p-0 font-16">
            <input type="password" name="reType" id="repass" class="form-control" style="width: 30rem" required>
          </div>
        </div>
        <div class="text-right">
          <button  type="submit" class="btn btn-app disabled" id="save">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#formApp').on('click change keyup',function () {
    $('#pass,#repass').removeClass('form-error');
    $('#save').removeClass('disabled');
    if($('#pass').val() != $('#repass').val()){
      $('#pass,#repass').addClass('form-error');
      $('#save').addClass('disabled');
    }
  });
</script>
@endsection