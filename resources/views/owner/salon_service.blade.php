@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold pink">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="row m-0 pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <div class="col p-0">
          <span class="text-bold font-20">Kelolah Salon</span>
        </div>
        <div class="col p-0 text-right">
          <a href="{{url('/owner/add/service-salon')}}" class="btn btn-app pt-1 pb-1 font-16"><i class="material-icons">add_circle_outline</i> Layanan</a>
        </div>
      </div>
      <div class="mt-3">
        <div class="row" style="margin: -3px;">
          @foreach($services as $service)
          <div class="col-sm-6 col-xl-4 col-lg-6 p-2">
            <div class="list-service">
              <img src="{{asset('images/service')}}/{{$service->sampul}}">
              <div class="list-service-title">
                <span>{{$service->deskripsi}}</span>
              </div>
              <div class="list-service-action">
                <a href="{{url('/detail/service',$service->id)}}"><i class="material-icons">visibility</i></a>
                <a href="{{url('/owner/edit/service-salon',$service->id)}}"><i class="material-icons">edit</i></a>
                <a href="" dataid="{{$service->id}}" class="delete" data-toggle="modal" data-target="#modalDelete"><i class="material-icons">delete_forever</i></a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Hapus Layanan</h4>
      </div>
      <div class="modal-body">
        Apakah Anda Yakin ingin menghapus layanan ini
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <a href="" type="button" class="btn btn-pink" id="confirm">Ya</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var id;
  $('.delete').click(function () {
    id = $(this).attr('dataid');
    $('#confirm').attr('href',`{{url('/owner/delete/service-salon/`+id+`')}}`);
  })
</script>
@endsection