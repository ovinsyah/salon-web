@extends('master.master_main')
@section('content')
<div class="col-home mt-5" id="formAppEdit">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('images/owner')}}/{{Auth::user()->image}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold pink">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2" style="border-bottom: 1px solid#d0d0d0;">
        <span class="text-bold font-20">Edit Layanan</span>
      </div>
      <form method="post" action="{{url('owner/update/service-salon',$service->id)}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Nama Layanan</div>
          <div class="col p-0 font-16">
            <input type="" name="nama" class="form-control" style="width: 30rem" required value="{{$service->nama}}">
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 text-bold" style="max-width:  16rem;">Sampul Layanan</div>
          <div class="col p-0">
            <div class="image-view-upload d-inline-block">
              <input name="image" type="file" accept="image/*" onchange="uploadimage()" id="imginput" class="hidden" />
              <img id="imgview" src="{{asset('images/service')}}/{{$service->sampul}}" class="sampul-upload"/>
              <div class="btn-upload-sampul text-center">
                <label class="mt-2 btn-change text-center" for="imginput">
                  <span class="btn">Pilih Gambar</span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="row m-0 mt-2">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Harga Layanan</div>
          <div class="col p-0 font-16">
            <input type="hidden" min="1" id="harga" name="harga" class="form-control" style="width: 20rem" required value="{{$service->harga}}">
            <input type="" min="1" id="tmpharga" class="form-control" style="width: 20rem" required value="Rp. {{number_format($service->harga,0,',','.')}}">
          </div>
        </div>
        <div class="row m-0 mt-4">
          <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Layanan Untuk</div>
          <div class="col p-0 font-16">
           <input type="radio" id="semua" value="semua" name="gender" {{($service->gender=="semua")?'checked':''}}>
           <label class="mr-3" for="semua">Semua</label>
           <input type="radio" id="pria" value="pria" name="gender" {{($service->gender=="pria")?'checked':''}}>
           <label class="mr-3" for="pria">Pria</label>
           <input type="radio" id="wanita" value="wanita" name="gender" {{($service->gender=="wanita")?'checked':''}}>
           <label for="wanita">Wanita</label>
         </div>
       </div>
       <div class="row m-0 mt-4">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Diskon</div>
        <div class="col p-0 font-16" id="disc" >
          <div class="mb-2">
            <input type="radio" id="no" name="disc" value=0  {{($service->diskon=="0")?'checked':''}}>
            <label for="no">Tidak</label>
          </div>
          <div class="mb-2">
            <input type="radio" id="yes" name="disc" value=1 {{($service->diskon!="0")?'checked':''}}>
            <label for="yes">Ya</label>
          </div>
          <div class="hidden" id="formDisc">
            <input type="number" name="val_disc" id="hargaDisc" class="form-control d-inline-block" style="width: 7rem;margin-left: 30px;" value="{{$service->diskon}}"> %
            <div id="hargaDiscText" style="margin-left: 30px;">Rp. {{number_format($service->harga-($service->diskon*$service->harga)/100,0,',','.')}}</div>
          </div>
        </div>
      </div>
      <div class="row m-0 mt-2">
        <div class="col p-0 pt-2 text-bold" style="max-width:  16rem;">Deskripsi Layanan</div>
        <div class="col p-0 font-16">
          <textarea class="form-control" rows="6" name="deskripsi">{{$service->deskripsi}}</textarea>
        </div>
      </div>
      <div class="text-right mt-5">
        <button type="submit" class="btn btn-app" id="confirm">Simpan</button>
      </div>
    </form>
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    check();
    $('#disc').on('click ',function () {
      check();
    });

    function check() {
      if($('#yes').is(':checked')){
        $('#formDisc').removeClass('hidden');
      }
      else{
        $('#formDisc').addClass('hidden');
      } 
    }
    $('#formAppEdit').on('click change keyup',function () {
      var harga =  $('#tmpharga').val();
      $('#harga').val(convertToAngka(harga));
      var tmpharga = $('#harga').val();

      if($('#harga').val() == 'NaN'){
        tmpharga = '0';
        $('#harga').val('0');
      }

      $('#tmpharga').val(convertToRupiah(tmpharga));
      var disc = $('#hargaDisc').val();
      var totaldisc = parseInt(tmpharga) - (parseInt(tmpharga)*(disc/100));
      $('#hargaDiscText').text(convertToRupiah(Math.round(totaldisc)));
    });

  });
</script>
@endsection