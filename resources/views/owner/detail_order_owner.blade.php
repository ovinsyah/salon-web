@extends('master.master_main')
@section('content')
<div class="col-home mt-5">
  <div class="row m-0 mb-5">
    <div class="col-md-3">
      <div>
        <img src="{{asset('img/salon.jpg')}}" class="profile-owner">
        <span class="ml-2 font-16 text-bold">{{Auth::user()->name}}</span>
      </div>
      <div class="mt-5">
        <a href="{{url('/owner/profile')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #ff5d41;top: 9px">account_box</i>
          <span class="font-14 text-bold">Profil Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/service-salon')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #44b5ff;top: 9px">store</i>
          <span class="font-14 text-bold">Kelolah Salon</span>
        </a>
      </div>
      <div class="mt-4">
        <a href="{{url('/owner/item-sold')}}">
          <i class="font-30 material-icons" style="font-size:  30px;color: #23d864;top: 9px">assignment_turned_in</i>
          <span class="font-14 text-bold pink">Item Terjual</span>
        </a>
      </div>
    </div>
    <div class="col-md-9 p-5 bg-white">
      <div class="pb-2 mb-4" style="border-bottom: 1px solid#d0d0d0;">
          <span class="text-bold font-20">Detail Pesanan {{$order->id}}</span>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Id order</div>
        <div class="col p-0 font-16">{{$order->id}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">No Invoice</div>
        <div class="col p-0 font-16">{{$order->invoice}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Nama Pembeli</div>
        <div class="col p-0 font-16">{{$order->user->name}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Nomor Telepon</div>
        <div class="col p-0 font-16">{{$order->telepon}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Kedatangan</div>
        <div class="col p-0 font-16">{{$order->jam_datang}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Nomor Antrian</div>
        <div class="col p-0 font-16">{{$order->no_antrian}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">Kode Claim</div>
        <div class="col p-0 font-16">{{$order->unik_code}}</div>
      </div>
      <div class="row m-0 mt-4">
        <div class="col p-0 text-bold" style="max-width:  16rem;">List Pesanan</div>
        <div class="col p-0 font-16">
          <ul class="list-group" style="box-shadow: none;">
            @foreach($order->list as $item)
          <li class="border-0 pl-0 pt-0 list-group-item d-flex justify-content-between align-items-center">
            <a href="{{url('detail/service',$item->layanan_id)}}">
            {{$item->layanan->nama}}
            </a>
            <span class="badge badge-primary badge-pill">{{$item->qty}}</span>
          </li>
          @endforeach

        </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#table_id').DataTable();
</script>
@endsection