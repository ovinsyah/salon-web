-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17 Jul 2018 pada 15.31
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salon`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `isi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `abouts`
--

INSERT INTO `abouts` (`id`, `isi`, `created_at`, `updated_at`) VALUES
(1, '<p>ini hanyalah halaman about bias gak ada</p><p> yang <b>istimewa</b></p>', '2018-06-03 07:00:20', '2018-06-03 00:36:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$tRf6Kgyv214vGafxrx29neJJmKkszZGq6LHn/hJAmlXhm3rc3JhsG', 'Khq3HWu6OK6B3yHJ7MZZYLy2lo7ap4CxjeRjRM8Ax9fHO8TyNe2yUCEWxvJH', '2018-05-30 16:34:57', '2018-05-30 16:34:57'),
(5, 'admin2', 'admin2@gmail.com', '$2y$10$r4yHxXDF3cQ5j3DMUrLSF.6laYKdE5Led9ceaPYScIMFYn7mjK4yG', '4IGfJ23iHltznrUMMGBewWOC9XZ9Dot5lhQGtF8KoAPe9DGsURG0rU7RmuJx', '2018-06-05 09:37:59', '2018-06-05 09:37:59'),
(6, 'admin9', 'admin9@gmail.com', '$2y$10$vatOQ262mo3gGZIQa2ONNusTZ6tj4e6LrILqUgzrk/xX58SMUABCa', 'DHTG9HjzUzj0dkJLyiPLMLgeJrclp159GXxrF2t476j848gQ8r9qqsQP4wkN', '2018-06-05 09:43:04', '2018-06-05 09:43:04'),
(7, 'admin5', 'admin5@gmail.com', '$2y$10$XHEivisj18/qMjpwH8u4AufHPeSNxtOXAeaRyyRmOrtrulOQ5bbje', NULL, '2018-06-05 09:46:39', '2018-06-05 09:46:39'),
(8, 'gusti', 'gusti.wandiira@gmail.com', '$2y$10$/ITXHE/f95i.YYfAlksi4e2cbEJWFlPuly45wIH/3vXGAhY6jOPs6', 'eEpKI6mwf5JP39AxBjvCuKGQqbVIIX4SydaR7faHUHWRRnaCM53SPmjdUJKE', '2018-06-25 01:09:01', '2018-06-25 01:09:01'),
(9, 'ryan', 'ryanricardo1996@gmail.com', '$2y$10$Jtp1/6J5w1kAttjresaZp.Z3V1ygqNwIT26WWfuNvd5Mdsls4DFE6', 'nz8e4SfExYxrs0M2cZryOmJjMAqMhBoEOA7PWMaM045PeYA11HwWA5kclmhJ', '2018-06-25 01:17:04', '2018-06-25 01:17:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `antrians`
--

CREATE TABLE `antrians` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `antrians`
--

INSERT INTO `antrians` (`id`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-06-27 17:00:00', '2018-07-12 17:49:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sampul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `articles`
--

INSERT INTO `articles` (`id`, `judul`, `sampul`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Article 1', '579971999.jpg', '<p>ini article</p>', '2018-06-08 22:11:00', '2018-06-08 22:11:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `layanans`
--

CREATE TABLE `layanans` (
  `id` int(10) UNSIGNED NOT NULL,
  `pemilik_id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sampul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon` tinyint(1) NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `layanans`
--

INSERT INTO `layanans` (`id`, `pemilik_id`, `nama`, `sampul`, `harga`, `diskon`, `gender`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 1, 'crambath susu mu', '547973187.jpg', 1200000, 1, 'semua', 'harga muah dengan bahan berkualitas ,dijamin  bosque...', '2018-06-08 09:23:04', '2018-06-10 19:49:00'),
(2, 4, 'mandi keringat', '813696989.jpg', 90000, 10, 'semua', 'mandi keringat demi mencari sesuap nasi yang sulit didapatkan pada kota milenial ini', '2018-06-11 21:20:45', '2018-06-27 03:54:02'),
(3, 4, 'potong janggut', '973868127.jpg', 50000, 0, 'pria', 'ubah janggutmu menjadi menarik dan enak dilihat', '2018-06-11 21:53:29', '2018-06-11 21:53:29'),
(4, 4, 'Cuci rambut', '948091223.jpg', 35000, 20, 'semua', 'ini cuci rambut', '2018-06-25 02:45:07', '2018-06-25 02:45:07'),
(5, 6, 'Cukur Rambut', '984765933.jpg', 300000, 20, 'semua', 'mantep ni  bosku', '2018-07-12 17:38:00', '2018-07-12 17:42:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_terjuals`
--

CREATE TABLE `list_terjuals` (
  `id` int(10) UNSIGNED NOT NULL,
  `layanan_id` int(10) UNSIGNED NOT NULL,
  `terjual_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `list_terjuals`
--

INSERT INTO `list_terjuals` (`id`, `layanan_id`, `terjual_id`, `qty`, `harga`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, 1188000, '2018-06-11 17:00:00', '2018-06-11 17:00:00'),
(2, 2, 5, 1, 81000, '2018-06-12 22:34:46', '2018-06-12 22:34:46'),
(3, 3, 5, 5, 50000, '2018-06-12 22:34:46', '2018-06-12 22:34:46'),
(4, 2, 6, 2, 81000, '2018-06-21 03:01:39', '2018-06-21 03:01:39'),
(5, 3, 6, 3, 50000, '2018-06-21 03:01:39', '2018-06-21 03:01:39'),
(6, 1, 7, 1, 1188000, '2018-06-22 02:30:32', '2018-06-22 02:30:32'),
(7, 2, 8, 1, 81000, '2018-06-25 01:26:36', '2018-06-25 01:26:36'),
(8, 1, 9, 3, 1188000, '2018-06-25 01:57:02', '2018-06-25 01:57:02'),
(9, 2, 10, 1, 81000, '2018-06-27 04:07:30', '2018-06-27 04:07:30'),
(10, 3, 10, 1, 50000, '2018-06-27 04:07:30', '2018-06-27 04:07:30'),
(11, 4, 10, 1, 28000, '2018-06-27 04:07:30', '2018-06-27 04:07:30'),
(16, 4, 18, 2, 28000, '2018-06-27 05:49:25', '2018-06-27 05:49:25'),
(17, 5, 19, 1, 240000, '2018-07-12 17:48:58', '2018-07-12 17:48:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_30_152553_create_admins_table', 1),
(4, '2018_05_30_152638_create_pemiliks_table', 1),
(5, '2018_06_03_061407_create_abouts_table', 2),
(6, '2018_06_03_061653_create_sliders_table', 2),
(7, '2018_06_03_061743_create_videos_table', 2),
(8, '2018_06_03_061819_create_no_rekenings_table', 2),
(9, '2018_06_03_062033_create_articles_table', 2),
(12, '2018_06_06_233628_create_layanans_table', 3),
(13, '2018_06_11_092406_create_terjuals_table', 4),
(15, '2018_06_12_021529_create_list_terjuals_table', 5),
(16, '2018_06_27_132805_create_ulasans_table', 6),
(17, '2018_06_28_075519_create_antrians_table', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `no_rekenings`
--

CREATE TABLE `no_rekenings` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `no_rekenings`
--

INSERT INTO `no_rekenings` (`id`, `nama`, `nomor`, `image`, `created_at`, `updated_at`) VALUES
(1, 'BRI', '23092370952', '449407546.png', '2018-06-21 03:03:54', '2018-06-21 03:03:54'),
(2, 'BNI', '1254153325', '704503854.jpg', '2018-06-21 03:04:05', '2018-06-21 03:04:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemiliks`
--

CREATE TABLE `pemiliks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'default.jpg',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_buka` time DEFAULT NULL,
  `jam_tutup` time DEFAULT NULL,
  `alamat` longtext COLLATE utf8mb4_unicode_ci,
  `latlng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pemiliks`
--

INSERT INTO `pemiliks` (`id`, `name`, `telepon`, `image`, `email`, `password`, `jam_buka`, `jam_tutup`, `alamat`, `latlng`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'owner', '08765436242', 'default.jpg', 'owner@gmail.com', '$2y$10$tRf6Kgyv214vGafxrx29neJJmKkszZGq6LHn/hJAmlXhm3rc3JhsG', '10:30:00', '17:30:49', 'Medan Mall, Pusat Pasar, Kota Medan, Sumatera Utara, Indonesia', '3.5929674000000005,98.68815569999992', 'we86KMXmCSkmscgIM9cj9V6XLkl0XpEgoWZ53hGR7xed9zwUjniGZojOCUVe', '2018-05-30 16:34:27', '2018-07-17 04:00:38'),
(4, 'owner2', '08125453646', '787213119.jpg', 'owner2@gmail.com', '$2y$10$66BGdqf8QzyLcxMdOH9fquHlaLW/MGjd57GTuVtPe7aKLBgoWXwIy', '00:00:00', '10:30:21', 'jl.baru dua', '3.5891272,98.6939443', 'G6Wep8eV9sTAUONwveJ2mv5gIpjBPty8ZbWYWHKLrTrSLArvZfvOSTovPOSR', '2018-06-05 09:42:16', '2018-06-11 22:20:54'),
(5, 'baber salon', NULL, 'default.jpg', 'ndhagrp@yahoo.com', '$2y$10$Xa6veWHf5PaLHVAOy..Mb.Z8qtzZngRtuZ0IKFN9TORFYyOl8eXFi', NULL, NULL, NULL, NULL, 'UMXYxQxo5iGPDcD1aUz8mba5V4Ob5ziQMl3DOFfya3rQVi0GK8FRtkvuawp0', '2018-06-25 01:26:47', '2018-06-25 01:26:47'),
(6, 'ovin salon', NULL, 'default.jpg', 'ovinsalon@mail.com', '$2y$10$KTF/.lXkGXzaYN1rKsrXAOfwAkOjqzbWt0XybTzNEoxztQDprpzjW', NULL, NULL, NULL, NULL, NULL, '2018-07-12 17:37:19', '2018-07-12 17:37:19'),
(7, 'Ovinsyah', '0812-1234-5678', 'default.jpg', 'ovinsyah@mail.com', '$2y$10$JAc7iO6KFYxwYIJMHf2.cOabuoC6YL.uy7SOAcWjsDP8Kn5HNeElK', '03:00:00', '09:00:00', 'Jl. Sipiso Piso, Mesjid, Medan Kota, Kota Medan, Sumatera Utara 20212, Indonesia', '3.5750913,98.687275', NULL, '2018-07-17 03:41:02', '2018-07-17 04:01:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sampul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sliders`
--

INSERT INTO `sliders` (`id`, `judul`, `sampul`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Style Rambut Masa Kini', '941160478.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p><p>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p><p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p><p>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</p><p>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p><p>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2018-06-23 05:46:23', '2018-07-12 16:14:41'),
(2, 'style rambut 2018', '399406309.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p><p>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p><p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p><p>consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</p><p>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</p><p>proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2018-06-29 13:17:37', '2018-07-12 16:15:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `terjuals`
--

CREATE TABLE `terjuals` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `pemilik_id` int(10) UNSIGNED NOT NULL,
  `telepon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_datang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `bukti` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_antrian` int(11) DEFAULT NULL,
  `unik_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `terjuals`
--

INSERT INTO `terjuals` (`id`, `user_id`, `pemilik_id`, `telepon`, `invoice`, `jam_datang`, `status`, `bukti`, `no_antrian`, `unik_code`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '5m85vb785', '06/20/2018 12:15 PM', 2, '353773511.jpg', 2, '634276', '2018-06-10 17:00:00', '2018-06-28 00:36:16'),
(5, 1, 4, NULL, '5b20acf6c299f', '06/13/2018 12:34 PM', 3, '530489039.jpg', 1, '876634', '2018-06-12 22:34:46', '2018-06-25 21:25:03'),
(6, 1, 4, NULL, '5b2b778362d27', '22-06-18 17:00', 2, '386266140.png', 1, '158361', '2018-06-21 03:01:39', '2018-06-27 21:12:41'),
(7, 1, 1, NULL, '5b2cc1b8b4fc1', '23-06-18 16:30', 2, '440273178.jpg', 3, '273762', '2018-06-22 02:30:32', '2018-06-28 00:49:52'),
(8, 6, 4, NULL, '5b30a73cb10fd', '25-06-18 15:26', 2, '155032588.jpg', 5, '658415', '2018-06-25 01:26:36', '2018-06-28 09:33:00'),
(9, 1, 1, NULL, '5b30ae5e56911', '27-06-18 10:00', 3, '416346541.jpg', 4, '711559', '2018-06-25 01:57:02', '2018-06-28 14:46:59'),
(10, 1, 4, '081534173781', '849112', '28-06-18 18:07', 4, NULL, NULL, NULL, '2018-06-27 04:07:29', '2018-06-28 14:36:59'),
(18, 1, 4, '0812345386576', '5b3387d55f835', '27-06-18 19:49', 0, NULL, NULL, NULL, '2018-06-27 05:49:25', '2018-06-27 05:49:25'),
(19, 7, 6, '0812', '5b47948abbf96', '13-07-18 00:44', 3, '147161928.png', 1, '628396', '2018-07-12 17:48:58', '2018-07-12 17:58:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ulasans`
--

CREATE TABLE `ulasans` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `layanan_id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ulasans`
--

INSERT INTO `ulasans` (`id`, `user_id`, `layanan_id`, `rating`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 4, 'kurang bagus', '2018-06-27 07:09:03', '2018-06-27 07:09:03'),
(2, 1, 3, 2, 'B aja ni', '2018-06-28 15:06:16', '2018-06-28 15:06:16'),
(3, 7, 5, 5, 'nice gan', '2018-07-12 17:58:26', '2018-07-12 17:58:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `email`, `telepon`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'pembeli 1', '558104604.jpg', 'user@gmail.com', '0813 7890 8980', '$2y$10$IMg0YN5meuG0PTf5E0tesuWmgqDeLZXJ8UNXtYWJI9IgJFtm11MRu', 'NpUsTDWdmjdKp3A8WAfwXhVIwszkqrZDPyPMAFTKe4aueawRj1RjzChXpLch', '2018-05-30 09:33:06', '2018-07-12 16:21:42'),
(5, 'user2', 'default.jpg', 'user2@gmail.com', '07465427', '$2y$10$/0WRdO4ZWsPZeM4vwwak2uFZg5q9aOLPJ8ym5JJYiLsjoP2mjPs8.', '6mNjwwNOMBrAD9u4KhZvBMn5xmvTUAszsOTtqHwbf9sl202RcXmw6gHCZoUt', '2018-06-05 09:28:18', '2018-06-05 09:28:18'),
(6, 'Wandira', 'default.jpg', 'gusti.wandiira@gmail.com', NULL, '$2y$10$nlIj61IX0tL3ISf.R54Rx.pusvjrVcc3cC8x3.Tt8hxsBkFTkmvHe', NULL, '2018-06-25 01:25:11', '2018-06-25 01:25:11'),
(7, 'ovin', 'default.jpg', 'ovin@mail.com', NULL, '$2y$10$gqa8XgA3p3Nxd9GIvB2boO31m5PZ2auqlk3lSaLiLuivTRymc65qy', NULL, '2018-07-12 17:33:12', '2018-07-12 17:33:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `videos`
--

INSERT INTO `videos` (`id`, `judul`, `video`, `created_at`, `updated_at`) VALUES
(2, 'ceramah ust.haikal', '621033388.mp4', '2018-06-23 04:22:22', '2018-06-23 04:22:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `antrians`
--
ALTER TABLE `antrians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layanans`
--
ALTER TABLE `layanans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `layanans_pemilik_id_foreign` (`pemilik_id`);

--
-- Indexes for table `list_terjuals`
--
ALTER TABLE `list_terjuals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `list_terjuals_layanan_id_foreign` (`layanan_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `no_rekenings`
--
ALTER TABLE `no_rekenings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pemiliks`
--
ALTER TABLE `pemiliks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pemiliks_email_unique` (`email`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terjuals`
--
ALTER TABLE `terjuals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ulasans`
--
ALTER TABLE `ulasans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ulasans_user_id_foreign` (`user_id`),
  ADD KEY `ulasans_layanan_id_foreign` (`layanan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `antrians`
--
ALTER TABLE `antrians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `layanans`
--
ALTER TABLE `layanans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `list_terjuals`
--
ALTER TABLE `list_terjuals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `no_rekenings`
--
ALTER TABLE `no_rekenings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pemiliks`
--
ALTER TABLE `pemiliks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `terjuals`
--
ALTER TABLE `terjuals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ulasans`
--
ALTER TABLE `ulasans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `layanans`
--
ALTER TABLE `layanans`
  ADD CONSTRAINT `layanans_pemilik_id_foreign` FOREIGN KEY (`pemilik_id`) REFERENCES `pemiliks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `list_terjuals`
--
ALTER TABLE `list_terjuals`
  ADD CONSTRAINT `list_terjuals_layanan_id_foreign` FOREIGN KEY (`layanan_id`) REFERENCES `layanans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `ulasans`
--
ALTER TABLE `ulasans`
  ADD CONSTRAINT `ulasans_layanan_id_foreign` FOREIGN KEY (`layanan_id`) REFERENCES `layanans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ulasans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
