<?php
use App\Layanan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Auth::routes();

Route::get('reset-antrian', function () {
	$antri=App\Antrian::first();
	$antri->position=1;
	$antri->updated_at=Carbon\Carbon::now()->toDateTimeString();
	$antri->save();
    return dd('sukses');
});
Route::get('admin', function () {
    return redirect('admin/index');
});
Route::get('owner', function () {
    return redirect('owner/profile');
});
Route::get('user', function () {
    return redirect('user/profile');
});

Route::get('/uniq', function () {
	$service=Layanan::all();
	$all='';
	foreach ($service as $v) {
		$all.=$v->nama.',';
	}
    return explode(",",$all);
});

// Route::get('/', 'HomeController@welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/login','AuthAdmin\LoginController@showLoginForm');
Route::post('admin/login','AuthAdmin\LoginController@login');
Route::get('admin/register','AuthAdmin\RegisterController@showRegistrationForm');
Route::post('admin/register','AuthAdmin\RegisterController@register');

Route::get('owner/login','AuthPemilik\LoginController@showLoginForm');
Route::post('owner/login','AuthPemilik\LoginController@login');
Route::get('owner/register','AuthPemilik\RegisterController@showRegistrationForm');
Route::post('owner/register','AuthPemilik\RegisterController@register');


Route::get('owner/logout','AuthPemilik\LoginController@logout_pemilik');
Route::get('admin/logout','AuthAdmin\LoginController@logout_admin');
Route::get('user/logout','Auth\LoginController@logout_user');


//========================Guest=========================
Route::get('/','GuestController@index');
Route::get('/article','GuestController@article');
Route::get('/detail/article/{id}','GuestController@detailarticle');
Route::get('/detail/slider/{id}','GuestController@detailslider');

Route::get('/discount','GuestController@discount');
Route::get('/detail/service/{id}','GuestController@detailservice');

Route::get('/salon','GuestController@salon');
Route::get('/detail/salon/{id}','GuestController@detailsalon');
Route::get('/about','GuestController@about');
Route::get('/search/{key}','GuestController@search');


//========================User=========================
Route::group(['prefix'=>'user','middleware'=>'auth:web'],function () {
	Route::get('/profile','UserController@index');
	Route::get('/edit/profile','UserController@editprofile');
	Route::get('/order','UserController@order');
	Route::get('/change-password','UserController@password');

	//crud profile
	Route::post('/update/profile','UserController@update');
	Route::post('/update/password','UserController@change_pass');

	//crud order
	Route::post('/create/order','UserController@create_order');
	Route::post('/upload/bukti/{id}','UserController@upload_bukti');
	Route::post('/verif/order/{id}','UserController@verif');
	Route::post('/ulasan/order/{id}','UserController@ulasan');
	Route::get('/cancel/order/{id}','UserController@cancel');
});

//========================Owner Salon=========================
Route::group(['prefix'=>'owner','middleware'=>'auth:pemilik'],function () {
	Route::get('/profile','OwnerController@profile');
	Route::get('/edit/profile','OwnerController@editprofile');
	Route::get('/service-salon','OwnerController@salonservice');
	Route::get('/change-password','OwnerController@password');

	Route::get('/item-sold','OwnerOrderController@index');
	Route::get('/history-selesai','OwnerOrderController@history');
	Route::get('/detail/order/{id}','OwnerOrderController@detail');

	Route::get('/add/service-salon','OwnerServiceController@add');
	Route::get('/edit/service-salon/{id}','OwnerServiceController@edit');

	//crud profile
	Route::post('/update/profile','OwnerController@update');
	Route::post('/update/password','ownerController@change_pass');

	//crud service
	Route::post('/create/service-salon','OwnerServiceController@create');
	Route::post('/update/service-salon/{id}','OwnerServiceController@update');
	Route::get('/delete/service-salon/{id}','OwnerServiceController@delete');
});


	//========================Admin=========================
Route::group(['prefix'=>'admin','middleware'=>'auth:admin'],function () {
	
	Route::get('/index','AdminController@index');

	//owner
	Route::get('/owner','AdminOwnerController@index');
	Route::get('/detail/owner/{id}','AdminOwnerController@detail');
	//user
	Route::get('/user','AdminUserController@index');
	Route::get('/detail/user/{id}','AdminUserController@detail');

	
	//slider
	Route::get('/slider','AdminSliderController@index');
	Route::get('/add/slider','AdminSliderController@add');
	Route::get('/edit/slider/{id}','AdminSliderController@edit');
	Route::get('/detail/slider/{id}','AdminSliderController@detail');

	Route::post('/create/slider','AdminSliderController@create');
	Route::post('/update/slider/{id}','AdminSliderController@update');
	Route::get('/delete/slider/{id}','AdminSliderController@delete');


	//artkel
	Route::get('/article','AdminArticleController@index');
	Route::get('/add/article','AdminArticleController@add');
	Route::get('/edit/article/{id}','AdminArticleController@edit');
	Route::get('/detail/article/{id}','AdminArticleController@detail');

	Route::post('/create/article','AdminArticleController@create');
	Route::post('/update/article/{id}','AdminArticleController@update');
	Route::get('/delete/article/{id}','AdminArticleController@delete');

	//video
	Route::get('/video','AdminVideoController@index');
	Route::get('/add/video','AdminVideoController@add');
	Route::get('/detail/video/{id}','AdminVideoController@detail');

	Route::post('/create/video','AdminVideoController@create');
	Route::get('/delete/video/{id}','AdminVideoController@delete');


	//rekening
	Route::get('/rekening','AdminRekeningController@index');
	Route::post('/create/rekening','AdminRekeningController@create');
	Route::post('/update/rekening/{id}','AdminRekeningController@update');
	Route::get('/delete/rekening/{id}','AdminRekeningController@delete');

	//about
	Route::get('/about','AdminAboutController@index');
	Route::get('/edit/about','AdminAboutController@edit');
	Route::post('/update/about','AdminAboutController@update');
	//order
	Route::get('/order/belum-bayar','AdminOrderController@belum');
	Route::get('/order/telah-bayar','AdminOrderController@telah');
	Route::get('/order/telah-diterima','AdminOrderController@telahditerima');
	Route::get('/order/selesai','AdminOrderController@selesai');
	Route::get('/order/batal','AdminOrderController@batal');

	Route::get('/detail/order/{id}','AdminOrderController@detail');

	//order crud
	Route::get('/verif/order/{id}','AdminOrderController@verif');


});