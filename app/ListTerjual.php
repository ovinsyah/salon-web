<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListTerjual extends Model
{
    public function layanan()
    {
        return $this->belongsTo(Layanan::class);
    }
}
