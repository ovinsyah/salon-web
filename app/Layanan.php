<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    public function pemilik()
    {
        return $this->belongsTo(Pemilik::class);
    }

     public function ulasan()
    {
        return $this->hasMany(Ulasan::class);
    }
}
