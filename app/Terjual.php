<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terjual extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function list()
    {
        return $this->hasMany(ListTerjual::class);
    }

     public function pemilik()
    {
        return $this->belongsTo(Pemilik::class);
    }

    
}
