<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Terjual;
use App\Antrian;
use Illuminate\Support\Carbon;

class AdminOrderController extends Controller
{
    public function belum()
    {
        $orders=Terjual::where('status',0)->get();
    	return view('admin.belum_bayar',compact('orders'));
    }
    public function telah()
    {
        $orders=Terjual::where('status',1)->get();//1 pending 
    	return view('admin.telah_bayar',compact('orders'));
    }
    public function telahditerima()
    {
        $orders=Terjual::where('status',2)->get();// 2 pembayran verif
    	return view('admin.telah_diterima',compact('orders'));
    }
    public function selesai()
    {
        $orders=Terjual::where('status',3)->get();
        return view('admin.selesai',compact('orders'));
    }
    public function batal()
    {
        $orders=Terjual::where('status',4)->get();
        return view('admin.batal',compact('orders'));
    }
    public function detail($id)
    {
        $order=Terjual::find($id);
        return view('admin.detail_order',compact('order'));
    }

    public function verif($id)
    {

       $order=Terjual::find($id);

       $last=Antrian::first();
        if (Carbon::now()->toDateString()==Carbon::parse($last->updated_at)->toDateString()) {
            $last->position= $last->position + 1;
            $last->save();
        }else{
            $last->position=1;
            $last->updated_at=Carbon::now()->toDateTimeString();
            $last->save();
        }
       $order->no_antrian=$last->position;
       $order->status=2;
       $order->unik_code=rand(111111,999999);
       $order->save();

       return back();
    }
}
