<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pemilik;
class AdminOwnerController extends Controller
{
    public function index()
    {
    	$owners=Pemilik::all();
    	return view('admin.owner',compact('owners'));
    }
    public function detail($id)
    {
    	$owners=Pemilik::find($id);
    	return view('admin.detail_owner',compact('owners'));
    }
}
