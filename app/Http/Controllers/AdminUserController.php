<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class AdminUserController extends Controller
{
    public function index()
    {
    	$users=User::all();
    	return view('admin.user',compact('users'));
    }
    public function detail($id)
    {
    	$user=User::find($id);
    	return view('admin.detail_user',compact('user'));
    }
}
