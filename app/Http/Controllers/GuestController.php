<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\About;
use App\Layanan;
use App\Pemilik;
use App\Slider;
use App\Video;
use App\Ulasan;
class GuestController extends Controller
{
    public function index()
    {
     $services=Layanan::all();
     $articles=Article::all();
     $videos=Video::all();
     $sliders=Slider::all();
    	return view('guest.index',compact('services','articles','videos','sliders'));
    }
    public function search($key)
    {
        $services=Layanan::where('nama','like', '%' . $key . '%')->get();
            return view('guest.search',compact('services','key'));
    }
    //article
    public function article()
    {
        $articles=Article::all();
            return view('guest.article',compact('articles'));
    }
    public function detailarticle($id)
    {
        $article=Article::find($id);
    	return view('guest.detail_article',compact('article'));
    }
    public function detailslider($id)
    {
        $slider=Slider::find($id);
        return view('guest.detail_slider',compact('slider'));
    }
    //service
    public function discount()
    {
        $services=Layanan::where('diskon','!=',0)->get();
        	return view('guest.discount',compact('services'));
    }
    public function detailservice($id)
    {
        $service=Layanan::find($id);
        $services=Layanan::where('pemilik_id',$service->pemilik_id)->where('id','!=',$id)->get();
        return view('guest.detail_service',compact('service','services'));
    }
    //salon
    public function salon()
    {
        $salons=Pemilik::all();
            return view('guest.salon',compact('salons'));
    }
    public function detailsalon($id)
    {
        $salon=Pemilik::find($id);
        return view('guest.detail_salon',compact('salon'));
    }
    //about
    public function about()
    {
        $about=About::first();
        return view('guest.about',compact('about'));
    }
}
