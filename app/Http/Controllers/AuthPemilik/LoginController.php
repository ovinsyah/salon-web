<?php

namespace App\Http\Controllers\AuthPemilik;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:pemilik')->except(['logout','logout_pemilik']);
    }

     public function showLoginForm()
    {
        return view('authPemilik.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);
         $credential= [
            'email' => $request->email,
            'password' => $request->password,
        ];

       if (Auth::guard('pemilik')->attempt($credential,$request->member)) {
           return redirect()->intended('/owner/profile') ;//suuces
       }
       return redirect()->back()->withInput($request->only('email','remember'))->withErrors(["email"=>"email tidak ada"]);//failed
    }

        public function logout_pemilik()
    {
        Auth::guard('pemilik')->logout();

        return redirect('/');
    }
}
