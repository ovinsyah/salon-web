<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Layanan;
use Auth;
use File;
use Hash;

class OwnerController extends Controller
{
    public function profile()
    {
        $services=Layanan::all();
    	return view ('owner.profile_owner',compact('services'));
    }
    public function editprofile()
    {
        $user=Auth::guard('pemilik')->user();
    	return view ('owner.edit_profile_owner',compact('user'));
    }
    public function salonservice()
    {
        $services=Layanan::where('pemilik_id',Auth::guard('pemilik')->user()->id)->get();
    	return view ('owner.salon_service',compact('services'));
    }
    public function password()
    {
        return view ('owner.change_password_owner',compact('service'));
    }

    public function update(Request $request)
    {
        $user=Auth::guard('pemilik')->user();
        $user->name=$request->name;
        $user->telepon=$request->telepon;
        $user->jam_buka=$request->jam_buka;
        $user->jam_tutup=$request->jam_tutup;
        $user->alamat=$request->alamat;
        $user->latlng=$request->latlng;

        if ($request->hasFile('image')) {
            $image_path = public_path().'/images/owner/'.$user->image;
            if(File::exists($image_path) && $user->image!='default.jpg'){unlink($image_path);}

            $file=$request->file('image');
                $ext = $file->getClientOriginalExtension();
                $filenames=rand(100000000,999999999).".".$ext;
                $file->move('images/owner',$filenames);
            $user->image=$filenames;
        }

        $user->save();
        return redirect('/owner/profile');
    }

    public function change_pass(Request $request)
    {
        $user=Auth::guard('pemilik')->user();
        if (Hash::check($request->old,$user->password)) {
            $user->password=bcrypt($request->new);
            $user->save();
            return redirect('/owner/profile')->with(['success' => 'berhasil merubah password']);
        }else{
            return redirect('/owner/profile')->with(['error' => 'gagal merubah password']);
        }
    }
}

