<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use File;

class AdminSliderController extends Controller
{
    public function index()
    {
        $sliders=Slider::all();
    	return view('admin.slider',compact('sliders'));
    }
    public function add()
    {
    	return view('admin.add_slider');
    }
    public function edit($id)
    {
        $slider=Slider::find($id);
    	return view('admin.edit_slider',compact('slider'));
    }
    public function detail($id)
    {
        $slider=Slider::find($id);
    	return view('admin.detail_slider',compact('slider'));
    }

     public function create(Request $request)
    {
        $slider= new Slider();
        $slider->judul=$request->judul;

            $file=$request->file('sampul');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/slider',$filenames);

        $slider->sampul=$filenames;
        $slider->deskripsi=$request->isi;
        $slider->save();
        return response()->json(['success add'=>$request->all()],200);
    }

    public function update(Request $request,$id)
    {
        $slider=Slider::find($id);
        $slider->judul=$request->judul;

        if ($request->hasFile('sampul')) {
            $file=$request->file('sampul');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/slider',$filenames);

            $image_path = public_path().'/images/slider/'.$slider->sampul;
            if(File::exists($image_path)){unlink($image_path);}
            $slider->sampul=$filenames;
        }
        $slider->deskripsi=$request->isi;
        $slider->save();
        return response()->json(['success update'=>$request->all()],200);
    }

    public function delete($id)
    {
        $slider=Slider::find($id);
          $image_path = public_path().'/images/slider/'.$slider->sampul;
            if(File::exists($image_path)){unlink($image_path);}
        $slider->delete();

        return back();
    }
}
