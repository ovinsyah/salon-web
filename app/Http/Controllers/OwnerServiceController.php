<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Layanan;
use File;
use Auth;
class OwnerServiceController extends Controller
{
    public function add()
    {
    	return view('owner.add_service_salon');
    }
    public function edit($id)
    {
    	 $service=Layanan::find($id);
    	return view('owner.edit_service_salon',compact('service'));
    }
    public function create(Request $request)
    {
    	$service= new Layanan();
        $service->nama=$request->nama;
    	$service->pemilik_id=Auth::guard('pemilik')->user()->id;

    	 	$file=$request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/service',$filenames);
    	$service->sampul=$filenames;

    	$service->harga=$request->harga;
    	$service->diskon=($request->disc==0)?$request->disc:$request->val_disc;
    	$service->gender=$request->gender;
    	$service->deskripsi=$request->deskripsi;
    	$service->save();
    	return redirect('/owner/service-salon');
    }

    public function update(Request $request,$id)
    {
    	$service=Layanan::find($id);
    	$service->nama=$request->nama;

    	 if ($request->hasFile('image')) {
          	  $file=$request->file('image');
              $ext = $file->getClientOriginalExtension();
              $filenames=rand(100000000,999999999).".".$ext;
                 $image_path=public_path().'/images/service/'.$service->sampul;
                 if (File::exists($image_path)){ unlink($image_path);}
              $file->move('images/service',$filenames);
    		$service->sampul=$filenames;
         }

    	$service->harga=$request->harga;
        $service->diskon=($request->disc==0)?$request->disc:$request->val_disc;
    	$service->gender=$request->gender;
    	$service->deskripsi=$request->deskripsi;
    	$service->save();

    	return redirect('/owner/service-salon');
    }

    public function delete($id)
    {
    	$service=Layanan::find($id);
    	 $image_path=public_path().'/images/service/'.$service->sampul;
                 if (File::exists($image_path)){ unlink($image_path);}
        $service->delete();

        return back();
    }

}
