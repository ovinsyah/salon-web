<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoRekening;
use File;
class AdminRekeningController extends Controller
{
    public function index()
    {
    	$rekenings=NoRekening::all();
    	return view('admin.rekening',compact('rekenings'));
    }

     public function create(Request $request)
    {
    	$rekening=new NoRekening();
    	$rekening->nama=$request->add_nama;
    	$rekening->nomor=$request->add_no;

    		$file=$request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/rekening',$filenames);
    	$rekening->image=$filenames;	

    	$rekening->save();
    	return response()->json(['succes add'=>$rekening],200);
    }

    public function update(Request $request,$id)
    {
    	$rekening=NoRekening::find($id);
    	$rekening->nama=$request->edit_nama;
    	$rekening->nomor=$request->edit_no;

        if ($request->hasFile('sampul')) {
    		$file=$request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/rekening',$filenames);

            $image_path = public_path().'/images/rekening/'.$rekening->image;
            if(File::exists($image_path)){unlink($image_path);}
    		$rekening->image=$filenames;	
    	}

    	$rekening->save();
    	return response()->json(['succes add'=>$rekening],200);
    }

      public function delete($id)
    {
    	$rekening=NoRekening::find($id);
    	 $image_path = public_path().'/images/rekening/'.$rekening->image;
            if(File::exists($image_path)){unlink($image_path);}
        $rekening->delete();

        return back();
    }
}
