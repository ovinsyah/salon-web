<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Terjual;
class OwnerOrderController extends Controller
{
    public function index ()
    {
    	$user=Auth::guard('pemilik')->user();
    	$orders=Terjual::where('pemilik_id',$user->id)->where('status',2)->get();;
    	return view('owner.item_sold',compact('user','orders'));
    }
    public function history ()
    {
        $user=Auth::guard('pemilik')->user();
        $orders=Terjual::where('pemilik_id',$user->id)->where('status',3)->get();;
        return view('owner.history_selesai',compact('user','orders'));
    }
    public function detail ($id)
    {
    	$order=Terjual::find($id);
		return view('owner.detail_order_owner',compact('order'));
    }
}
