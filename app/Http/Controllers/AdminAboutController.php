<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;

class AdminAboutController extends Controller
{
    public function index()
    {
    	$about=About::first();
    	return view('admin.about',compact('about'));
    }
    public function edit()
    {
    	$about=About::first();
    	return view('admin.edit_about',compact('about'));
    }

      public function update(Request $request)
    {
    	$about=About::first();
    	$about->isi=$request->isi;
    	$about->save();
    	return response()->json(['bisa'=>$request->all()],200);
    }
}
