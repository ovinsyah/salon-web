<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use File;
class AdminArticleController extends Controller
{
    public function index()
    {
        $articles=Article::all();
    	return view('admin.article',compact('articles'));
    }
    public function add()
    {
    	return view('admin.add_article');
    }
    public function edit($id)
    {
        $article=Article::find($id);
    	return view('admin.edit_article',compact('article'));
    }
    public function detail($id)
    {
        $article=Article::find($id);
    	return view('admin.detail_article',compact('article'));
	}

    public function create(Request $request)
    {
        $article= new Article();
        $article->judul=$request->judul;

            $file=$request->file('sampul');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/article',$filenames);

        $article->sampul=$filenames;
        $article->deskripsi=$request->isi;
        $article->save();
        return response()->json(['success add'=>$request->all()],200);
    }

    public function update(Request $request,$id)
    {
        $article=Article::find($id);
        $article->judul=$request->judul;

        if ($request->hasFile('sampul')) {
            $file=$request->file('sampul');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('images/article',$filenames);

            $image_path = public_path().'/images/article/'.$article->sampul;
            if(File::exists($image_path)){unlink($image_path);}
            $article->sampul=$filenames;
        }
        $article->deskripsi=$request->isi;
        $article->save();
        return response()->json(['success update'=>$request->all()],200);
    }

    public function delete($id)
    {
        $article=Article::find($id);
          $image_path = public_path().'/images/article/'.$article->sampul;
            if(File::exists($image_path)){unlink($image_path);}
        $article->delete();

        return back();
    }
}
