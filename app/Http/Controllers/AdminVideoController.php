<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use File;

class AdminVideoController extends Controller
{
    public function index()
    {
        $videos=Video::all();
    	return view('admin.video',compact('videos'));
    }
    public function add()
    {
    	return view('admin.add_video');
    }
    public function detail($id)
    {
        $video=Video::find($id);
    	return view('admin.detail_video',compact('video'));
	}

    public function create(Request $request)
    {

        $video= new Video();
        $video->judul=$request->judul;

            $file=$request->file('video');
            $ext = $file->getClientOriginalExtension();
            $filenames=rand(100000000,999999999).".".$ext;
            $file->move('video',$filenames);

        $video->video=$filenames;
        $video->save();

        return response()->json(["data"=>$request->all()],200);
    }

     public function delete($id)
    {
        $video=Video::find($id);
          $video_path = public_path().'/video/'.$video->video;
            if(File::exists($video_path)){unlink($video_path);}
        $video->delete();

        return back();
    }
}
