<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use File;
use Hash;
use App\Terjual;
use App\Layanan;
use App\ListTerjual;
use App\NoRekening;
use App\Ulasan;
class UserController extends Controller
{
    public function index()
    {
        $user=Auth::user();
    	return view('user.profile_user',compact('user'));
    }
    public function editprofile()
    {
        $user=Auth::user();
    	return view('user.edit_profile_user',compact('user'));
    }
    public function order()
    {
        $orders=Terjual::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $rekenings=NoRekening::all();
    	return view('user.order_user',compact('orders','rekenings'));
    }
    public function password()
    {
        return view('user.change_password_user');
    }

    public function update(Request $request)
    {
        $user=Auth::user();
        $user->name=$request->name;
        $user->telepon=$request->telepon;

        if ($request->hasFile('image')) {
            $image_path = public_path().'/images/user/'.$user->image;
            if(File::exists($image_path) && $user->image!='default.jpg'){unlink($image_path);}

            $file=$request->file('image');
                $ext = $file->getClientOriginalExtension();
                $filenames=rand(100000000,999999999).".".$ext;
                $file->move('images/user',$filenames);
            $user->image=$filenames;
        }
        $user->save();

       return redirect('/user/profile');
    }

    public function change_pass(Request $request)
    {
        $user=Auth::user();
        if (Hash::check($request->old,$user->password)) {
            $user->password=bcrypt($request->new);
            $user->save();
            return redirect('/user/profile')->with(['success' => 'berhasil merubah password']);
        }else{
            return redirect('/user/profile')->with(['error' => 'gagal merubah password']);
        }
    }

    public function create_order(Request $request)
    {

        $req=$request->json()->all();
        $lenArr=count($req);
        $data=$req[$lenArr-1];

        $order=new Terjual();
        $order->user_id=$data['user'];
        $order->pemilik_id=$data['pemilik'];
        $order->jam_datang=$data['jam'];
        $order->telepon=$data['telepon'];
        $order->status=0;
        $order->invoice=uniqid();
        if($order->save()){
            foreach ($req as $i => $item) {
               // return response()->json(["isi"=>$req,"order"=>$order,"item"=>$item['id']],200);
                if ($i < $lenArr-1){ 
                    $service=Layanan::find($item['id']);
                       $list=new ListTerjual();
                       $list->layanan_id=$item['id'];
                       $list->terjual_id=$order->id;
                       $list->harga= ($service->diskon == 0)? $service->harga : ($service->harga-($service->diskon*$service->harga)/100);
                       $list->qty=$item['jumlah'];
                       $list->save();
                   }
            }
        }
            return response()->json(["isi"=>$req,"order"=>$order],200);
    }

    public function upload_bukti(Request $request,$id)
    {
        $order=Terjual::find($id);

        $file=$request->file('image');
                $ext = $file->getClientOriginalExtension();
                $filenames=rand(100000000,999999999).".".$ext;
                $file->move('images/bukti',$filenames);
            $order->bukti=$filenames;

        $order->status=1;
        $order->save();

        return response()->json(["order"=>$order,"file"=>$ext],200);
    }

    public function verif(Request $request,$id)
    {
        $order=Terjual::find($id);
        if($order->unik_code==$request->code){
            $order->status=3;
            $order->save();
            return response()->json(["order"=>$order],200);
        }else{
            return response()->json(["order"=>"Not Match"],500);
        }

    }

     public function cancel($id)
    {
        $order=Terjual::find($id);
        $order->status=4;
        $order->save();
            return back();
    }

    public function ulasan(Request $request,$id)
    {
        $ulasan= new Ulasan();
        $ulasan->user_id=Auth::user()->id;
        $ulasan->layanan_id=$id;
        $ulasan->rating=$request->rate;
        $ulasan->comment=$request->komentar;
        $ulasan->save();

        return response()->json(["sukses"=>$request->all()],200);
    }
}
