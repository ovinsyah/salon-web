<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
     public function service()
    {
        return $this->belongsTo(Layanan::class,'layanan_id');
    }
     public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
