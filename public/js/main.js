function uploadimage() {
	var output = document.getElementById('imgview');
	console.log(output)
	output.src = URL.createObjectURL(event.target.files[0]);
};
function uploadimage2() {
	var output = document.getElementById('imgview2');
	output.src = URL.createObjectURL(event.target.files[0]);
};
$(document).ready( function () {
	$('#formApp').on('click change keyup',function () {
		var status = 1;
		// $('.note-frame.panel').removeClass('form-error');
		$('#confirm').addClass('disabled');
		$('#judul,#imginput,#vid').each(function () {
			$(this).removeClass('form-error');
			if($(this).val() == ''){
				status = 0;
				$(this).addClass('form-error');
			}
		});
/*		if($('.note-editable p').text() == ''){
			status = 0;
			$('.note-frame.panel').addClass('form-error');
		}*/
		if(status == 1){
			$('#confirm').removeClass('disabled');
		}
	});

	$('#formAppEdit').on('click change keyup',function () {
		var status = 1;
		$('#confirm').addClass('disabled');
		$('#judul').each(function () {
			$(this).removeClass('form-error');
			if($(this).val() == ''){
				status = 0;
				$(this).addClass('form-error');
			}
		});
		if(status == 1){
			$('#confirm').removeClass('disabled');
		}
	});

/*	$('#confirm').click(function () {
		$(this).addClass('disabled');
	});*/

	$('.add').click(function () {
		var numb = $(this).parent().children('.numb');
		var angka = $(numb).val();
		angka++;
		$(numb).val(angka);
	});
	$('.min').click(function () {
		var numb = $(this).parent().children('.numb');
		var angka = $(numb).val();
		if(angka >0){
			angka--;
			$(numb).val(angka);
		}
	});
});
function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
}
function convertToAngka(rupiah)
{
	return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
}